package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;

public class SweepUiResultJson extends ErrorJson
{
	public int power;//扫荡一次消耗的体力值
	public int entryTimes;//可挑战次数
	public int itemNum;//扫荡券个数
	public int sweepTimes;//可连续挑战次数
	public int bNum;//场次
	public int md;//关卡id
	public int getPower() {
		return power;
	}
	public void setPower(int power) {
		this.power = power;
	}
	public int getEntryTimes() {
		return entryTimes;
	}
	public void setEntryTimes(int entryTimes) {
		this.entryTimes = entryTimes;
	}
	public int getItemNum() {
		return itemNum;
	}
	public void setItemNum(int itemNum) {
		this.itemNum = itemNum;
	}
	public int getSweepTimes() {
		return sweepTimes;
	}
	public void setSweepTimes(int sweepTimes) {
		this.sweepTimes = sweepTimes;
	}
	public int getBNum() {
		return bNum;
	}
	public void setBNum(int bNum) {
		this.bNum = bNum;
	}
	public int getMd() {
		return md;
	}
	public void setMd(int md) {
		this.md = md;
	}
	
	

}

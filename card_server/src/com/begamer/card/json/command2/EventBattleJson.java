package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class EventBattleJson extends BasicJson
{
	public int id;//fbeventId	
	public int eid;//eventid

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEid() {
		return eid;
	}

	public void setEid(int eid) {
		this.eid = eid;
	}
	
	
}

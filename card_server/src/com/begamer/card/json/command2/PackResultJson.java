package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.PackElement;

public class PackResultJson extends ErrorJson{
	public List<PackElementJson> pejs;
	public List<PackElement> list;
	public List<PackElement> list2;//被动技能
	public int buyTimes;
	public String [] allSelect;
	public List<PackElementJson> getPejs() {
		return pejs;
	}
	public void setPejs(List<PackElementJson> pejs) {
		this.pejs = pejs;
	}
	public List<PackElement> getList() {
		return list;
	}
	public void setList(List<PackElement> list) {
		this.list = list;
	}
	public int getBuyTimes()
	{
		return buyTimes;
	}
	public void setBuyTimes(int buyTimes)
	{
		this.buyTimes = buyTimes;
	}
	public String[] getAllSelect() {
		return allSelect;
	}
	public void setAllSelect(String[] allSelect) {
		this.allSelect = allSelect;
	}
	public List<PackElement> getList2()
	{
		return list2;
	}
	public void setList2(List<PackElement> list2)
	{
		this.list2 = list2;
	}
	
}

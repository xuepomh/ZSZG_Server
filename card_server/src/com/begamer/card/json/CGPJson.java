package com.begamer.card.json;

import java.util.List;

public class CGPJson {
	
	public List<PackElement> passiveSkills;

	public List<PackElement> getPassiveSkills()
	{
		return passiveSkills;
	}

	public void setPassiveSkills(List<PackElement> passiveSkills)
	{
		this.passiveSkills = passiveSkills;
	}
}

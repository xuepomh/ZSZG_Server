package com.begamer.card.json.command;


public class LoginJson
{
	public String uid;
	public String psd;
	public String nickname;
	public int other;//==0无信息,1断线重连==//
	public String platform;
	
	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getPsd() {
		return psd;
	}

	public void setPsd(String psd) {
		this.psd = psd;
	}
	
	public int getOther()
	{
		return other;
	}

	public void setOther(int other)
	{
		this.other = other;
	}

	public String getPlatform()
	{
		return platform;
	}

	public void setPlatform(String platform)
	{
		this.platform = platform;
	}

	public String getNickname()
	{
		return nickname;
	}

	public void setNickname(String nickname)
	{
		this.nickname = nickname;
	}

}

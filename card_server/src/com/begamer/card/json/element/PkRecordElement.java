package com.begamer.card.json.element;

import com.begamer.card.cache.Cache;
import com.begamer.card.common.util.StringUtil;
import com.begamer.card.model.pojo.Player;

public class PkRecordElement
{
	public int type;//(0,挑战别人,1,别人来挑战)
	public String name;//pk玩家的name
	public int rank;//0,排名不变,x提升或者下降至x名
	public int time;//记录时间(小时为单位)
	public int r;//(1胜利,2失败)
	public PkRecordElement(String record)
	{
		if(record !=null)
		{
			String [] temp =record.split("&");
			this.type =StringUtil.getInt(temp[0]);
			int id =StringUtil.getInt(temp[1]);
			Player player =Cache.getInstance().getPlayer(id);
			this.name =player.getName();
			this.rank =StringUtil.getInt(temp[2]);
			this.r =StringUtil.getInt(temp[3]);
			int t =(int)(System.currentTimeMillis()-StringUtil.getTimeStamp(temp[4]))/1000;
			if(t%60==0)
			{
				t =t/3600;
			}
			else
			{
				t =(t/3600)+1;
			}
			this.time =t;
		}
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public int getR() {
		return r;
	}
	public void setR(int r) {
		this.r = r;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}

package com.begamer.card.controller;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.begamer.card.cache.Cache;
import com.begamer.card.cache.MailThread;
import com.begamer.card.cache.PlayerInfo;
import com.begamer.card.common.Constant;
import com.begamer.card.common.dao.hibernate.CommDao;
import com.begamer.card.common.util.RequestUtil;
import com.begamer.card.common.util.StringUtil;
import com.begamer.card.common.util.binRead.BinReader;
import com.begamer.card.common.util.binRead.CardData;
import com.begamer.card.common.util.binRead.EquipData;
import com.begamer.card.common.util.binRead.EventData;
import com.begamer.card.common.util.binRead.ItemsData;
import com.begamer.card.common.util.binRead.MazeData;
import com.begamer.card.common.util.binRead.MissionData;
import com.begamer.card.common.util.binRead.PassiveSkillData;
import com.begamer.card.common.util.binRead.RechargeData;
import com.begamer.card.common.util.binRead.SkillData;
import com.begamer.card.common.util.binRead.UnitSkillData;
import com.begamer.card.json.PayBackJson;
import com.begamer.card.log.ManagerLogger;
import com.begamer.card.model.dao.ActivityInfoDao;
import com.begamer.card.model.dao.GmPlayerDao;
import com.begamer.card.model.dao.TimeMailDao;
import com.begamer.card.model.pojo.Activity;
import com.begamer.card.model.pojo.ActivityInfo;
import com.begamer.card.model.pojo.Announce;
import com.begamer.card.model.pojo.Card;
import com.begamer.card.model.pojo.Equip;
import com.begamer.card.model.pojo.EventDrop;
import com.begamer.card.model.pojo.Item;
import com.begamer.card.model.pojo.Mail;
import com.begamer.card.model.pojo.Manager;
import com.begamer.card.model.pojo.PageUtil;
import com.begamer.card.model.pojo.PassiveSkill;
import com.begamer.card.model.pojo.PkRank;
import com.begamer.card.model.pojo.Player;
import com.begamer.card.model.pojo.Skill;
import com.begamer.card.model.pojo.SpeciaMail;
import com.begamer.card.model.pojo.TimeMail;
import com.begamer.card.model.pojo.ZcMail;
import com.begamer.card.model.view.CardView;
import com.begamer.card.model.view.EquipView;
import com.begamer.card.model.view.EventView;
import com.begamer.card.model.view.ItemView;
import com.begamer.card.model.view.MazeView;
import com.begamer.card.model.view.MissionView;
import com.begamer.card.model.view.PassiveSkillView;
import com.begamer.card.model.view.PublicMailView;
import com.begamer.card.model.view.SkillView;

public class GmController extends AbstractMultiActionController {
	
	private static Logger managerLogger = ManagerLogger.logger;
	private static Logger logger = Logger.getLogger(GmController.class);
	@Autowired
	private GmPlayerDao gmPlayerDao;
	@Autowired
	private TimeMailDao timeMailDao;
	@Autowired
	private CommDao commDao;
	@Autowired
	private ActivityInfoDao activityInfoDao;
	
	private Player player;
	private StringBuilder sb;
	
	/***************************************************************************
	 * 分页查询
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public ModelAndView allByPage(HttpServletRequest request, HttpServletResponse response)
	{
		
		try
		{
			// 获取当前页数
			int currentpage = RequestUtil.GetParamInteger(request, "pageCurrent");
			if (currentpage == 0)
			{
				currentpage = 1;
			}
			// 获取数据的总条数
			int count = gmPlayerDao.findAllPlayer().size();
			// 创建分页实体
			PageUtil page = new PageUtil(count, 10, currentpage);
			List<Player> list = gmPlayerDao.getAllByPage(page.getCurrentPage(), page.getPageSize());
			
			// 优先从在线玩家取
			if (list != null)
			{
				for (int k = 0; k < list.size(); k++)
				{
					Player player = list.get(k);
					PlayerInfo pi = Cache.getInstance().getPlayerInfoForGm(player.getId());
					if (pi != null)
					{
						list.remove(k);
						list.add(k, pi.player);
					}
				}
			}
			String[] num = new String[page.getTotalPage()];
			String href = "gm.htm?action=allByPage&pageCurrent=";
			request.setAttribute("page", page);
			request.setAttribute("num", num);
			request.setAttribute("myherf", href);
			request.setAttribute("list", list);
			
			/** 统计玩家在线人数* */
			int online = Cache.getInstance().getOnLinePlayer();
			/** 统计注册玩家人数* */
			int userNum = gmPlayerDao.getUserNum();
			
			request.setAttribute("online", online);
			request.setAttribute("userNum", userNum);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return new ModelAndView("/gm/gPerInfo.vm");
	}
	
	public ModelAndView one(HttpServletRequest request, HttpServletResponse response)
	{
		
		try
		{
			int p_id = Integer.parseInt(request.getParameter("p_id"));
			// 优先从在线玩家取
			PlayerInfo pi = Cache.getInstance().getPlayerInfoForGm(p_id);
			if (pi != null)
			{
				player = pi.player;
			}
			else
			{
				player = gmPlayerDao.getOneByP_id(p_id);
			}
			request.setAttribute("player", player);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return new ModelAndView("/gm/gupdateper.vm");
	}
	
	public ModelAndView fight(HttpServletRequest request, HttpServletResponse response)
	{
		
		try
		{
			int p_id = Integer.parseInt(request.getParameter("p_id"));
			
			// 优先从在线玩家取
			PlayerInfo pi = Cache.getInstance().getPlayerInfoForGm(p_id);
			if (pi != null)
			{
				player = pi.player;
			}
			else
			{
				player = gmPlayerDao.getOneByP_id(p_id);
			}
			
			request.setAttribute("player", player);
			// 已开 关卡
			MissionData missionData = MissionData.getUnlockData(player.getMissionId());
			MissionView missionView = new MissionView();
			missionView.setId(missionData.id);
			missionView.setName(missionData.name);
			if (missionData != null)
			{
				request.setAttribute("missionData", missionView);
			}
			else
			{
				request.setAttribute("missionData", "已开通所有关卡");
			}
			
			// pvp
			PkRank pkrank = gmPlayerDao.getPkRankByP_id(p_id);
			request.setAttribute("pkrank", pkrank);
			// 副本
			if (player.getFBnum1() != "" && player.getFBnum1().trim().length() > 0)
			{
				String[] str = player.getFBnum1().split(";");
				List<EventView> events = new ArrayList<EventView>();
				for (int i = 0; i < str.length; i++)
				{
					String[] num = str[i].split(",");
					for (int n = 0; n < num.length - 1; n++)
					{
						EventView fbnum = new EventView();
						EventData event = EventData.getEventData(Integer.parseInt(num[n]));
						fbnum.setName(event.name);// 副本
						fbnum.setNum(Integer.parseInt(num[n + 1]));// 次数
						events.add(fbnum);
					}
				}
				request.setAttribute("events", events);
			}
			
			// 迷宫
			if (player.getMaze() != "" && player.getMaze().trim().length() > 0)
			{
				String[] mgs = player.getMaze().split(",");
				List<MazeView> mazeView = new ArrayList<MazeView>();
				for (int a = 0; a < mgs.length; a++)
				{
					String[] mg = mgs[a].split("-");
					MazeView maze = new MazeView();
					MazeData mazedata = MazeData.getMazeData(Integer.parseInt(mg[0]));
					maze.setName(mazedata.name);// 迷宫
					maze.setScene(mazedata.scene);// 位置
					maze.setNum(Integer.parseInt(mg[2]));// 次数
					if (Integer.parseInt(mg[3]) == 1)// 付费标示
					{
						maze.setPay("已付费");
					}
					else
					{
						maze.setPay("未付费");
						mazeView.add(maze);
					}
				}
				request.setAttribute("mazes", mazeView);
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return new ModelAndView("/gm/fight.vm");
	}
	
	public ModelAndView fbFight(HttpServletRequest request, HttpServletResponse response)
	{
		
		int p_id = Integer.parseInt(request.getParameter("p_id"));
		// 优先从在线玩家取
		PlayerInfo pl = Cache.getInstance().getPlayerInfoForGm(p_id);
		if (pl != null)
		{
			player = pl.player;
			
		}
		else
		{
			player = gmPlayerDao.getOneByP_id(p_id);
		}
		
		request.setAttribute("player", player);
		// 所有副本
		List<EventView> eventList = new ArrayList<EventView>();
		List<EventData> fbs = EventData.getEvents();
		for (EventData eventData : fbs)
		{
			EventView eventView = EventView.creatEventView(eventData.ID, eventData.name, 0);
			eventList.add(eventView);
		}
		request.setAttribute("fbs", eventList);
		// 已有副本
		if (player.getFBnum1() != "")
		{
			String[] str = player.getFBnum1().split(";");
			List<EventView> events = new ArrayList<EventView>();
			for (int i = 0; i < str.length; i++)
			{
				String[] num = str[i].split(",");
				for (int n = 0; n < num.length - 1; n++)
				{
					EventData event = EventData.getEventData(Integer.parseInt(num[n]));
					EventView eventView = EventView.creatEventView(Integer.parseInt(num[n]), event.name, Integer.parseInt(num[n + 1]));
					events.add(eventView);
				}
			}
			request.setAttribute("events", events);
		}
		return new ModelAndView("/gm/upfbFight.vm");
	}
	
	public ModelAndView mazeFight(HttpServletRequest request, HttpServletResponse response)
	{
		int p_id = Integer.parseInt(request.getParameter("p_id"));
		// 优先从在线的玩家取
		PlayerInfo pl = Cache.getInstance().getPlayerInfoForGm(p_id);
		if (pl != null)
		{
			player = pl.player;
		}
		else
		{
			player = gmPlayerDao.getOneByP_id(p_id);
		}
		
		request.setAttribute("player", player);
		// 所有迷宫
		List<MazeView> mazevList = new ArrayList<MazeView>();
		List<MazeData> mds = MazeData.getMazeDataj(player.getMissionId());
		for (MazeData mazeData : mds)
		{
			MazeView mazeView = MazeView.createMazeView(mazeData.id, mazeData.name, mazeData.scene, 0, "");
			mazevList.add(mazeView);
		}
		request.setAttribute("mds", mazevList);
		// 玩家已经解锁的迷宫
		if (player.getMaze() != "" && player.getMaze().trim().length() > 0)
		{
			String[] mgs = player.getMaze().split(",");
			List<MazeView> mazeList = new ArrayList<MazeView>();
			for (int a = 0; a < mgs.length; a++)
			{
				String[] mg = mgs[a].split("-");
				MazeData mazedata = MazeData.getMazeData(Integer.parseInt(mg[0]));
				MazeView mazeView = null;
				if (Integer.parseInt(mg[3]) == 1)// 付费标示
				{
					mazeView = MazeView.createMazeView(Integer.parseInt(mg[0]), mazedata.name, mazedata.scene, Integer.parseInt(mg[2]), "已付费");// id~迷宫~位置~次数~付费标示
				}
				else
				{
					mazeView = MazeView.createMazeView(Integer.parseInt(mg[0]), mazedata.name, mazedata.scene, Integer.parseInt(mg[2]), "未付费");
				}
				mazeList.add(mazeView);
				
			}
			request.setAttribute("mazes", mazeList);
		}
		return new ModelAndView("/gm/upmazeFight.vm");
	}
	
	public ModelAndView missionFight(HttpServletRequest request, HttpServletResponse response)
	{
		
		int p_id = Integer.parseInt(request.getParameter("p_id"));
		// 优先从在线的玩家取
		PlayerInfo pl = Cache.getInstance().getPlayerInfoForGm(p_id);
		if (pl != null)
		{
			player = pl.player;
		}
		else
		{
			player = gmPlayerDao.getOneByP_id(p_id);
		}
		request.setAttribute("player", player);
		// 已解锁
		MissionData missionData = MissionData.getUnlockData(player.getMissionId());
		MissionView missionView = new MissionView();
		missionView.setId(missionData.id);
		missionView.setName(missionData.name);
		request.setAttribute("missionData", missionView);
		// 所有关卡
		List<MissionView> missionList = new ArrayList<MissionView>();
		List<MissionData> missionDatas = MissionData.getMissionDatas();
		for (MissionData missionData2 : missionDatas)
		{
			MissionView missionView2 = new MissionView();
			missionView2.setId(missionData2.id);
			missionView2.setName(missionData2.name);
			missionList.add(missionView2);
		}
		request.setAttribute("missionDatas", missionList);
		return new ModelAndView("/gm/upmissionFight.vm");
	}
	
	public ModelAndView pvpFight(HttpServletRequest request, HttpServletResponse response)
	{
		
		int p_id = Integer.parseInt(request.getParameter("p_id"));
		// 优先从在线的玩家取
		PlayerInfo pl = Cache.getInstance().getPlayerInfoForGm(p_id);
		if (pl != null)
		{
			player = pl.player;
		}
		else
		{
			player = gmPlayerDao.getOneByP_id(p_id);
		}
		request.setAttribute("player", player);
		PkRank pkrank = gmPlayerDao.getPkRankByP_id(p_id);
		request.setAttribute("pkrank", pkrank);
		return new ModelAndView("/gm/uppvpFight.vm");
	}
	
	public ModelAndView upfbFight(HttpServletRequest request, HttpServletResponse response)
	{
		
		int p_id = Integer.parseInt(request.getParameter("p_id"));
		// 优先从在线的玩家取
		PlayerInfo pl = Cache.getInstance().getPlayerInfoForGm(p_id);
		if (pl != null)
		{
			player = pl.player;
		}
		else
		{
			player = gmPlayerDao.getOneByP_id(p_id);
		}
		// 获得数据
		String fbid = request.getParameter("fbname");
		String fbnum = request.getParameter("fbnum");
		
		EventData ed = EventData.getEventData(Integer.parseInt(fbid));
		Manager manager = (Manager) request.getSession().getAttribute(Constant.LOGIN_USER);
		managerLogger.info(manager.getName() + "|修改了玩家：[" + player.getName() + "] 的[" + ed.name + "]副本的次数" + "|登录IP" + request.getRemoteAddr());
		sb = new StringBuilder();
		boolean find = false;
		if (player.getFBnum1() != null && player.getFBnum1().trim().length() > 0)
		{
			String[] str = player.getFBnum1().split(";");// (1001,1)(1002,1),(1003,1)
			for (int i = 0; i < str.length; i++)
			{// 3
				String[] s = str[i].split(",");// 1001,1
				if (s[0].equals(fbid))
				{// 如果添加次数
					find = true;
					Integer num = Integer.parseInt(s[1]) + Integer.parseInt(fbnum);
					s[0] = fbid.toString();
					s[1] = num.toString();
				}
				sb.append(s[0] + "," + s[1] + ";");
			}
		}
		if (!find)
		{
			sb.append(fbid + "," + fbnum + ";");
		}
		player = new Player();
		player.setId(p_id);
		player.setFBnum1(sb.toString());
		gmPlayerDao.upfbFight(player);
		
		this.fight(request, response);
		return new ModelAndView("/gm/fight.vm");
	}
	
	public ModelAndView upmazeFight(HttpServletRequest request, HttpServletResponse response)
	{
		
		int p_id = Integer.parseInt(request.getParameter("p_id"));
		// 优先从在线的玩家取
		PlayerInfo pl = Cache.getInstance().getPlayerInfoForGm(p_id);
		if (pl != null)
		{
			player = pl.player;
		}
		else
		{
			player = gmPlayerDao.getOneByP_id(p_id);
		}
		// 获得数据
		String mazeId = request.getParameter("mazeId");
		String mazeNum = request.getParameter("mazeNum");
		MazeData md = MazeData.getMazeData(Integer.parseInt(mazeId));
		Manager manager = (Manager) request.getSession().getAttribute(Constant.LOGIN_USER);
		managerLogger.info(manager.getName() + "|修改了玩家：[" + player.getName() + "] 的[" + md.name + "]迷宫的次数为" + mazeNum + "|登录IP" + request.getRemoteAddr());
		Integer expense = md.expense;
		sb = new StringBuilder();
		boolean find = false;
		if (player.getMaze() != null && player.getMaze().trim().length() > 0)
		{
			String[] str = player.getMaze().split(",");// 迷宫-位置-次数-付费标识(0未付，1已付)，迷宫-位置-次数-付费重置次数
			for (int i = 0; i < str.length; i++)
			{
				String[] s = str[i].split("-");
				if (s[0].equals(mazeId))
				{// 如果添加次数
					find = true;
					Integer num = Integer.parseInt(s[2]) + Integer.parseInt(mazeNum);
					s[0] = mazeId.toString();
					s[2] = num.toString();
				}
				sb.append(s[0] + "-" + s[1] + "-" + s[2] + "-" + s[3] + ",");
			}
		}
		if (!find)
		{
			sb.append(mazeId + "-" + md.scene + "-" + mazeNum + "-" + expense.toString() + ",");
		}
		player = new Player();
		player.setId(p_id);
		player.setMaze(sb.toString());
		gmPlayerDao.upmazeFight(player);
		
		this.fight(request, response);
		return new ModelAndView("/gm/fight.vm");
	}
	
	public ModelAndView upmissionFight(HttpServletRequest request, HttpServletResponse response)
	{
		
		int p_id = Integer.parseInt(request.getParameter("p_id"));
		int missionId = Integer.parseInt(request.getParameter("missionid"));
		// 判断是否在线
		PlayerInfo pl = Cache.getInstance().getPlayerInfoForGm(p_id);
		if (pl != null)
		{
			player = pl.player;
			pl.player.setMissionId(missionId);
		}
		else
		{
			player = gmPlayerDao.getOneByP_id(p_id);
			player.setMissionId(missionId);
			gmPlayerDao.upmissionFight(player);
		}
		
		MissionData missionData = MissionData.getMissionData(missionId);
		Manager manager = (Manager) request.getSession().getAttribute(Constant.LOGIN_USER);
		managerLogger.info(manager.getName() + "|开启玩家：[" + player.getName() + "] 的关卡至[" + missionData.name + "]" + "|登录IP" + request.getRemoteAddr());
		this.fight(request, response);
		return new ModelAndView("/gm/fight.vm");
	}
	
	public ModelAndView uppvpFight(HttpServletRequest request, HttpServletResponse response)
	{
		
		try
		{
			int p_id = Integer.parseInt(request.getParameter("p_id"));
			int pkNum = Integer.parseInt(request.getParameter("pkNum"));
			player = gmPlayerDao.getOneByP_id(p_id);
			// 判断是否在线
			PlayerInfo pi = Cache.getInstance().getPlayerInfoForGm(p_id);
			if (pi != null)
			{
				PkRank pkRank2 = Cache.getInstance().getRankById(p_id);
				pkRank2.setPkNum(pkNum);
				
			}
			else
			{
				PkRank pkrank = new PkRank();
				pkrank.setPlayerId(p_id);
				pkrank.setPkNum(pkNum);
				gmPlayerDao.uppvpFight(pkrank);
			}
			
			// ManagerLogInputDB.setMDC(request, player);
			Manager manager = (Manager) request.getSession().getAttribute(Constant.LOGIN_USER);
			managerLogger.info(manager.getName() + "|修改了玩家：[" + player.getName() + "] 的PVP挑战次数");
			this.fight(request, response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return new ModelAndView("/gm/fight.vm");
	}
	
	public ModelAndView update(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		// 判断玩家是否在线
		try
		{
			// 获取数据
			int playerId = RequestUtil.GetParamInteger(request, "id");
			String name = RequestUtil.GetParamString(request, "name", "");
			int power = RequestUtil.GetParamInteger(request, "power");
			int level = RequestUtil.GetParamInteger(request, "level");
			int gold = RequestUtil.GetParamInteger(request, "gold");
			int crystal = RequestUtil.GetParamInteger(request, "crystal");
			int crystalPay = RequestUtil.GetParamInteger(request, "crystalPay");
			int kopoint = RequestUtil.GetParamInteger(request, "kopoint");
			int vipCost = RequestUtil.GetParamInteger(request, "vipCost");
			int runeNum = RequestUtil.GetParamInteger(request, "runeNum");
			int diamond = RequestUtil.GetParamInteger(request, "diamond");
			int pvpHonor = RequestUtil.GetParamInteger(request, "pvpHonor");
			
			// 获取缓存中的数据
			PlayerInfo pi = Cache.getInstance().getPlayerInfoForGm(playerId);
			if (pi != null)// 玩家在线 修改缓存中的数据
			{
				pi.player.setName(name);
				pi.player.setPower(power);
				pi.player.setGold(gold);
				pi.player.setCrystal(crystal);
				pi.player.setCrystalPay(crystalPay);
				pi.player.setLevel(level);
				pi.player.setKopoint(kopoint);
				pi.player.setVipCost(vipCost);
				pi.player.updateVipLevel();
				pi.player.setRuneNum(runeNum);
				pi.player.setDiamond(diamond);
				pi.player.setPvpHonor(pvpHonor);
			}
			else
			{
				// 玩家不在线 修改数据库中数据
				Player player = new Player();
				player.setId(playerId);
				player.setName(name);
				player.setPower(power);
				player.setGold(gold);
				player.setCrystal(crystal);
				player.setCrystalPay(crystalPay);
				player.setLevel(level);
				player.setKopoint(kopoint);
				player.setVipCost(vipCost);
				player.setRuneNum(runeNum);
				player.setDiamond(diamond);
				player.setPvpHonor(pvpHonor);
				gmPlayerDao.updatePlayer(player);
			}
			managerLogger.info(getManager(request).getName() + "|修改了：[" + player.getName() + "] 的等级:" + level + "|体力:" + power + "|金币：" + gold + "|钻石:" + crystal + "|ko积分：" + kopoint + "|vip经验：" + vipCost + "|符文值:" + runeNum + "|金罡心:" + diamond + "|pvp荣誉点:" + pvpHonor + "|登录IP" + request.getRemoteAddr());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return allByPage(request, response);
	}
	
	public ModelAndView upNewPlayer(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		int p_id = StringUtil.getInt(request.getParameter("p_id"));
		
		// 获取缓存中的数据
		PlayerInfo pi = Cache.getInstance().getPlayerInfoForGm(p_id);
		if (pi != null)// 玩家在线 修改缓存中的数据
		{
			pi.player.setNewPlayerType(100);
		}
		else
		{
			gmPlayerDao.upNewPlayer(p_id);
		}
		return one(request, response);
	}
	
	public ModelAndView item(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			int p_id = Integer.parseInt(request.getParameter("p_id"));
			Player player1 = null;
			List<Item> items = null;
			List<Card> cards = null;
			List<Equip> equips = null;
			List<PassiveSkill> passiveSkills = null;
			List<Skill> skills = null;
			// 优先从在线玩家取
			PlayerInfo pi = Cache.getInstance().getPlayerInfoForGm(p_id);
			if (pi != null)
			{
				player1 = pi.player;
				items = pi.getItems();
				cards = pi.getCards();
				equips = pi.getEquips();
				passiveSkills = pi.getPassiveSkillls();
				skills = pi.getSkills();
			}
			else
			{
				player1 = gmPlayerDao.getOneByP_id(p_id);
				items = gmPlayerDao.getItemByPlayerId(p_id);
				cards = gmPlayerDao.getCardByP_id(p_id);
				equips = gmPlayerDao.getEquipByP_id(p_id);
				passiveSkills = gmPlayerDao.getPassiveSkillByP_id(p_id);
				skills = gmPlayerDao.getSkillByP_id(p_id);
			}
			request.setAttribute("player", player1);
			// 杂物
			List<ItemView> itemView = new ArrayList<ItemView>();
			for (int i = 0; i < items.size(); i++)
			{
				Item item = items.get(i);
				ItemsData itemsData = ItemsData.getItemsData(item.getItemId());
				ItemView itemView2 = new ItemView();
				itemView2.setId(item.getItemId());
				itemView2.setName(itemsData.name);
				itemView2.setPile(item.getPile());
				itemView.add(itemView2);
			}
			request.setAttribute("items", itemView);
			// 卡牌
			List<CardView> cardView = new ArrayList<CardView>();
			for (int c = 0; c < cards.size(); c++)
			{
				Card card = (Card) cards.get(c);
				CardData cd = CardData.getData(card.getCardId());
				CardView cardView2 = new CardView();
				cardView2.setId(card.getCardId());
				cardView2.setName(cd.name);
				cardView2.setLevel(card.getLevel());
				cardView2.setCurExp(card.getCurExp());
				cardView.add(cardView2);
			}
			request.setAttribute("cards", cardView);
			// 装备
			List<EquipView> equipView = new ArrayList<EquipView>();
			for (int e = 0; e < equips.size(); e++)
			{
				Equip equip = (Equip) equips.get(e);
				EquipData ed = EquipData.getData(equip.getEquipId());
				EquipView equipView2 = EquipView.creatEquipView(equip.getEquipId(), ed.name, ed.level);
				equipView.add(equipView2);
			}
			request.setAttribute("equips", equipView);
			// 被动技能
			List<PassiveSkillView> passiveSkillView = new ArrayList<PassiveSkillView>();
			for (int p = 0; p < passiveSkills.size(); p++)
			{
				PassiveSkill passiveSkill = (PassiveSkill) passiveSkills.get(p);
				PassiveSkillData psd = PassiveSkillData.getData(passiveSkill.getPassiveSkillId());
				PassiveSkillView passiveSkillView2 = PassiveSkillView.ceatePassiveSkillView(passiveSkill.getPassiveSkillId(), psd.name, psd.describe, passiveSkill.getCurExp());
				passiveSkillView.add(passiveSkillView2);
			}
			request.setAttribute("passiveSkills", passiveSkillView);
			// 主动技能
			List<SkillView> skillView = new ArrayList<SkillView>();
			for (int s = 0; s < skills.size(); s++)
			{
				Skill skill = (Skill) skills.get(s);
				SkillData sd = SkillData.getData(skill.getSkillId());
				SkillView skillView2 = SkillView.createSkillView(skill.getSkillId(), sd.name, skill.getLevel(), sd.description);
				skillView.add(skillView2);
			}
			request.setAttribute("skills", skillView);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return new ModelAndView("/gm/playerItem.vm");
	}
	
	public ModelAndView addItem(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			// 获取数据
			int itemId = RequestUtil.GetParamInteger(request, "item");
			int pile = RequestUtil.GetParamInteger(request, "pile");
			int playerId = RequestUtil.GetParamInteger(request, "p_id");
			// 获取缓存中的数据
			PlayerInfo pi = Cache.getInstance().getPlayerInfoForGm(playerId);
			if (pi != null)// 玩家在线
			{
				pi.addItem(itemId, pile);
			}
			else
			{
				Item item = Item.createItem(playerId, itemId, pile);
				gmPlayerDao.addItemByPlayerId(item);
			}
			Manager manager = (Manager) request.getSession().getAttribute(Constant.LOGIN_USER);
			Player p = gmPlayerDao.getOneByP_id(playerId);
			managerLogger.info(manager.getName() + "|给玩家：[" + p.getName() + "] 增加了：" + pile + "个" + itemId + "|登录IP" + request.getRemoteAddr());
			this.item(request, response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return new ModelAndView("/gm/playerItem.vm");
	}
	
	public ModelAndView allItem(HttpServletRequest request, HttpServletResponse response)
	{
		
		try
		{
			int p_id = Integer.parseInt(request.getParameter("p_id"));
			// 优先从在线玩家取
			PlayerInfo playerInfo = Cache.getInstance().getPlayerInfoForGm(p_id);
			List<Item> items = null;
			List<ItemView> itemsDatas1 = new ArrayList<ItemView>();
			if (playerInfo != null)
			{
				player = playerInfo.player;
				items = playerInfo.getItems();
			}
			else
			{
				player = gmPlayerDao.getOneByP_id(p_id);
				items = gmPlayerDao.getItemByPlayerId(p_id);
			}
			// 玩家已有的item
			for (int i = 0; i < items.size(); i++)
			{
				Item item = items.get(i);
				ItemsData itemsData = ItemsData.getItemsData(item.getItemId());
				ItemView itemView = new ItemView();
				itemView.setPile(item.getPile());
				itemView.setId(item.getItemId());
				itemView.setName(itemsData.name);
				itemsDatas1.add(itemView);
			}
			// 所有ITEM
			List<ItemsData> itemsDatas = ItemsData.getAllItemsData();
			List<ItemView> itemList = new ArrayList<ItemView>();
			for (ItemsData itemsData : itemsDatas)
			{
				ItemView itemView = ItemView.createItemView(itemsData.id, itemsData.name, itemsData.pile);
				itemList.add(itemView);
			}
			request.setAttribute("itemsDatas", itemList);
			request.setAttribute("list", itemsDatas1);
			request.setAttribute("player", player);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return new ModelAndView("/gm/addPlayerItem.vm");
	}
	
	public ModelAndView card(HttpServletRequest request, HttpServletResponse response)
	{
		
		// 获得数据
		int p_id = Integer.parseInt(request.getParameter("p_id"));
		// 优先从在线玩家中取
		PlayerInfo pInfo = Cache.getInstance().getPlayerInfoForGm(p_id);
		List<Card> cards = null;
		if (pInfo != null)
		{
			player = pInfo.player;
			cards = pInfo.getCards();
		}
		else
		{
			player = gmPlayerDao.getOneByP_id(p_id);
			cards = gmPlayerDao.getCardByP_id(p_id);
		}
		
		request.setAttribute("player", player);
		List<CardView> cardView = new ArrayList<CardView>();
		for (int c = 0; c < cards.size(); c++)
		{
			Card card = (Card) cards.get(c);
			CardData cd = CardData.getData(card.getCardId());
			CardView cardView2 = CardView.createCardView(card.getCardId(), cd.name, card.getLevel(), card.getCurExp());
			cardView.add(cardView2);
		}
		request.setAttribute("cards", cardView);
		List<CardData> cardDatas = CardData.getAllCardDatas();
		List<CardView> cardList = new ArrayList<CardView>();
		for (CardData cardData : cardDatas)
		{
			CardView cardView2 = new CardView();
			cardView2.setName(cardData.name);
			cardView2.setId(cardData.id);
			cardList.add(cardView2);
		}
		request.setAttribute("cardDatas", cardList);
		return new ModelAndView("/gm/upCard.vm");
	}
	
	public ModelAndView equip(HttpServletRequest request, HttpServletResponse response)
	{
		
		int p_id = Integer.parseInt(request.getParameter("p_id"));
		// 优先从在线玩家中取
		PlayerInfo pInfo = Cache.getInstance().getPlayerInfoForGm(p_id);
		List<Equip> equips = null;
		if (pInfo != null)
		{
			player = pInfo.player;
			equips = pInfo.getEquips();
		}
		else
		{
			player = gmPlayerDao.getOneByP_id(p_id);
			equips = gmPlayerDao.getEquipByP_id(p_id);
		}
		
		request.setAttribute("player", player);
		
		List<EquipView> equips1 = new ArrayList<EquipView>();
		for (int e = 0; e < equips.size(); e++)
		{
			Equip equip = (Equip) equips.get(e);
			EquipData ed = EquipData.getData(equip.getEquipId());
			EquipView equipView = EquipView.creatEquipView(equip.getEquipId(), ed.name, equip.getLevel());
			equips1.add(equipView);
		}
		request.setAttribute("equips", equips1);
		// 所有装备
		List<EquipData> equipDatas = EquipData.getAllEquipDatas();
		List<EquipView> equipViewList = new ArrayList<EquipView>();
		for (EquipData equipData : equipDatas)
		{
			EquipView equipView = EquipView.creatEquipView(equipData.index, equipData.name, equipData.level);
			equipViewList.add(equipView);
		}
		request.setAttribute("equipDatas", equipViewList);
		return new ModelAndView("/gm/upEquip.vm");
	}
	
	public ModelAndView passiveSkill(HttpServletRequest request, HttpServletResponse response)
	{
		
		int p_id = Integer.parseInt(request.getParameter("p_id"));
		// 优先从在线玩家取
		PlayerInfo pInfo = Cache.getInstance().getPlayerInfoForGm(p_id);
		List<PassiveSkill> passiveSkills = null;
		if (pInfo != null)
		{
			player = pInfo.player;
			passiveSkills = pInfo.getPassiveSkillls();
		}
		else
		{
			player = gmPlayerDao.getOneByP_id(p_id);
			passiveSkills = gmPlayerDao.getPassiveSkillByP_id(p_id);
		}
		request.setAttribute("player", player);
		List<PassiveSkillView> passiveSkills1 = new ArrayList<PassiveSkillView>();
		for (int p = 0; p < passiveSkills.size(); p++)
		{
			PassiveSkill passiveSkill = (PassiveSkill) passiveSkills.get(p);
			PassiveSkillData psd = PassiveSkillData.getData(passiveSkill.getPassiveSkillId());
			PassiveSkillView passiveSkillView = PassiveSkillView.ceatePassiveSkillView(passiveSkill.getPassiveSkillId(), psd.name, psd.describe, passiveSkill.getCurExp());
			passiveSkills1.add(passiveSkillView);
		}
		request.setAttribute("passiveSkills", passiveSkills1);
		// 获取全部被动技能
		List<PassiveSkillData> passiveSkillDatas = PassiveSkillData.getAllDatas();
		List<PassiveSkillView> pasList = new ArrayList<PassiveSkillView>();
		for (PassiveSkillData passiveSkill : passiveSkillDatas)
		{
			PassiveSkillView passiveSkillView = PassiveSkillView.ceatePassiveSkillView(passiveSkill.index, passiveSkill.name, passiveSkill.describe, passiveSkill.level);
			pasList.add(passiveSkillView);
		}
		request.setAttribute("passiveSkillDatas", pasList);
		return new ModelAndView("/gm/upPassiveSkill.vm");
	}
	
	public ModelAndView skill(HttpServletRequest request, HttpServletResponse response)
	{
		
		int p_id = Integer.parseInt(request.getParameter("p_id"));
		// 优先从在线玩家获取
		PlayerInfo pInfo = Cache.getInstance().getPlayerInfoForGm(p_id);
		List<Skill> skills = null;
		if (pInfo != null)
		{
			player = pInfo.player;
			skills = pInfo.getSkills();
		}
		else
		{
			player = gmPlayerDao.getOneByP_id(p_id);
			skills = gmPlayerDao.getSkillByP_id(p_id);
		}
		
		request.setAttribute("player", player);
		
		List<SkillView> skills1 = new ArrayList<SkillView>();
		for (int s = 0; s < skills.size(); s++)
		{
			Skill skill = (Skill) skills.get(s);
			SkillData sd = SkillData.getData(skill.getSkillId());
			SkillView skillView = SkillView.createSkillView(skill.getSkillId(), sd.name, skill.getLevel(), sd.description);
			skills1.add(skillView);
		}
		request.setAttribute("skills", skills1);
		List<SkillView> skiList = new ArrayList<SkillView>();
		List<SkillData> skillDatas = SkillData.getSkillDatas();
		for (SkillData skillData : skillDatas)
		{
			SkillView skillView = SkillView.createSkillView(skillData.index, skillData.name, skillData.level, skillData.description);
			skiList.add(skillView);
		}
		request.setAttribute("skillDatas", skiList);
		return new ModelAndView("/gm/upSkill.vm");
	}
	
	public ModelAndView addCard(HttpServletRequest request, HttpServletResponse response)
	{
		int playerId = Integer.parseInt(request.getParameter("p_id"));
		int cardId = Integer.parseInt(request.getParameter("cid"));
		PlayerInfo pi = Cache.getInstance().getPlayerInfoForGm(playerId);
		if (pi != null)
		{
			player = pi.player;
			pi.addCard(cardId, 1);
		}
		else
		{
			// 获取数据
			player = gmPlayerDao.getOneByP_id(playerId);
			Card c = Card.createCard(playerId, cardId, 1);
			gmPlayerDao.addCard(c);
		}
		Manager manager = (Manager) request.getSession().getAttribute(Constant.LOGIN_USER);
		managerLogger.info(manager.getName() + "|给玩家：[" + player.getName() + "] 增加了卡牌：" + cardId + "|登录IP" + request.getRemoteAddr());
		return this.item(request, response);
	}
	
	public ModelAndView addEquip(HttpServletRequest request, HttpServletResponse response)
	{
		int equipId = Integer.parseInt(request.getParameter("eid"));
		int playerId = Integer.parseInt(request.getParameter("p_id"));
		PlayerInfo pi = Cache.getInstance().getPlayerInfoForGm(playerId);
		if (pi != null)
		{
			player = pi.player;
			pi.addEquip(equipId);
		}
		else
		{
			player = gmPlayerDao.getOneByP_id(playerId);
			Equip e = Equip.createEquip(playerId, equipId);
			gmPlayerDao.addEquip(e);
		}
		Manager manager = (Manager) request.getSession().getAttribute(Constant.LOGIN_USER);
		managerLogger.info(manager.getName() + "|给玩家：[" + player.getName() + "] 增加了装备:" + equipId + "|登录IP" + request.getRemoteAddr());
		return this.item(request, response);
	}
	
	public ModelAndView addPassiveSkill(HttpServletRequest request, HttpServletResponse response)
	{
		int playerId = Integer.parseInt(request.getParameter("p_id"));
		int passiveSkillId = Integer.parseInt(request.getParameter("pid"));
		PlayerInfo pi = Cache.getInstance().getPlayerInfoForGm(playerId);
		if (pi != null)
		{
			player = pi.player;
			pi.addPassiveSkill(passiveSkillId);
		}
		else
		{
			player = gmPlayerDao.getOneByP_id(playerId);
			PassiveSkill pskill = PassiveSkill.createPassiveSkill(playerId, passiveSkillId);
			gmPlayerDao.addPassiveSkill(pskill);
		}
		Manager manager = (Manager) request.getSession().getAttribute(Constant.LOGIN_USER);
		managerLogger.info(manager.getName() + "|给玩家：[" + player.getName() + "] 添加了被动技能:" + passiveSkillId + "|登录IP" + request.getRemoteAddr());
		return this.item(request, response);
	}
	
	public ModelAndView addSkill(HttpServletRequest request, HttpServletResponse response)
	{
		int playerId = Integer.parseInt(request.getParameter("p_id"));
		int skillId = Integer.parseInt(request.getParameter("sid"));
		PlayerInfo pi = Cache.getInstance().getPlayerInfoForGm(playerId);
		if (pi != null)
		{
			player = pi.player;
			pi.addSkill(skillId, 1);
		}
		else
		{
			player = gmPlayerDao.getOneByP_id(playerId);
			Skill skill = Skill.createSkill(playerId, skillId, 1);
			gmPlayerDao.addSkill(skill);
		}
		Manager manager = (Manager) request.getSession().getAttribute(Constant.LOGIN_USER);
		managerLogger.info(manager.getName() + "|给玩家：[" + player.getName() + "] 添加了主动技能：" + skillId + "|登录IP" + request.getRemoteAddr());
		return this.item(request, response);
	}
	
	// ***********条件查询玩家************//
	public ModelAndView inquireCenter(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException
	{
		int type = Integer.parseInt(request.getParameter("type"));
		String keyword = request.getParameter("keyword");
		if (type == 1)
		{
			mhP_id(request, keyword);
		}
		if (type == 2)
		{
			String name = java.net.URLDecoder.decode(keyword, "UTF-8");
			if (name != "" && name.trim().length() > 0)
			{
				mhp_name(request, name);
			}
			else
			{
				return allByPage(request, response);
			}
		}
		if (type == 3)
		{
			int p_rank = StringUtil.getInt(keyword);
			getPlayerByPRank(request, response, p_rank);
			return new ModelAndView("statistic/playerrank.vm");
		}
		if (type == 4)
		{
			int level = StringUtil.getInt(keyword);
			getPlayerByPLevel(request, level);
			return new ModelAndView("statistic/playerlevel.vm");
		}
		if (type == 5)
		{
			String name = java.net.URLDecoder.decode(keyword, "UTF-8");
			if (name != "" && name.trim().length() > 0)
			{
				getPlayerByPname(request, name);
			}
			else
			{
				return allByPage(request, response);
			}
		}
		if (type == 6)
		{
			getPlayerById(request, keyword);
		}
		if (type == 7)
		{
			int vipCost = StringUtil.getInt(keyword);
			getPlayerByVipCost(request, vipCost);
		}
		request.setAttribute("type", type);
		return new ModelAndView("/gm/onePlayer.vm");
	}
	
	/** 游戏ID查询 **/
	public void getPlayerById(HttpServletRequest request, String keyword)
	{
		int id = StringUtil.getInt(keyword);
		try
		{
			Player player = null;
			List<Player> pList = gmPlayerDao.findAllPlayer();
			for (Player p : pList)
			{
				int pid = 0x0f000000 | p.getId();
				if (pid == id)
				{
					// 优先从在线玩家获取
					PlayerInfo pInfo = Cache.getInstance().getPlayerInfoForGm(p.getId());
					if (pInfo != null)
					{
						player = pInfo.player;
					}
					else
					{
						player = p;
					}
				}
			}
			if (player != null)
			{
				request.setAttribute("player", player);
			}
			else
			{
				request.setAttribute("msg", "没有找到相关角色!!");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	// ***********p_id查询************//
	public void mhP_id(HttpServletRequest request, String keyword)
	{
		int p_id = StringUtil.getInt(keyword);
		// 优先从在线玩家获取
		PlayerInfo pInfo = Cache.getInstance().getPlayerInfoForGm(p_id);
		if (pInfo != null)
		{
			player = pInfo.player;
		}
		else
		{
			player = gmPlayerDao.getOneByP_id(p_id);
		}
		
		if (player != null)
		{
			request.setAttribute("player", player);
		}
		else
		{
			request.setAttribute("msg", "没有找到相关角色!!");
		}
		
	}
	
	// ***********模糊p_name查询************//
	public void mhp_name(HttpServletRequest request, String name)
	{
		try
		{
			// 获取当前页数
			int currentpage = RequestUtil.GetParamInteger(request, "pageCurrent");
			if (currentpage == 0)
			{
				currentpage = 1;
			}
			// 获取数据的总条数
			int count = gmPlayerDao.mhP_name(name).size();
			// 创建分页实体
			PageUtil page = new PageUtil(count, 10, currentpage);
			// 数据库查名字匹配的,查出来一个集合
			List<Player> list = gmPlayerDao.mhP_name(page.getCurrentPage(), page.getPageSize(), name);
			for (int i = 0; i < list.size(); i++)
			{
				player = list.get(i);
				// 优先从在线玩家获取
				PlayerInfo pInfo = Cache.getInstance().getPlayerInfoForGm(player.getId());
				if (pInfo != null)
				{
					list.remove(i);
					list.add(i, pInfo.player);
				}
			}
			String[] num = new String[page.getTotalPage()];
			// String href = "gm.htm?action=mhp_name&p_name=" + name +
			// "&pageCurrent=";
			request.setAttribute("page", page);
			request.setAttribute("num", num);
			// request.setAttribute("myherf", href);
			request.setAttribute("name", name);
			if (list.size() > 0 && list != null)
			{
				request.setAttribute("list", list);
			}
			else
			{
				request.setAttribute("msg", "没有找到相关角色!!");
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	// ***********查询pvp前N名玩家************//
	public void getPlayerByPRank(HttpServletRequest request, HttpServletResponse response, int p_rank)
	{
		HashMap<Integer, PkRank> pkMap = Cache.getInstance().getPkRankMap2();
		List<PkRank> list = new ArrayList<PkRank>();
		for (int k = 1; k <= p_rank; k++)
		{
			PkRank pkRank = pkMap.get(k);
			if (pkRank != null)
			{
				list.add(pkRank);
			}
		}
		managerLogger.info(getManager(request).getName() + "|查看竞技场排行" + p_rank + "以上的玩家" + "|登录IP：" + request.getRemoteAddr());
		request.setAttribute("list", list);
		request.setAttribute("prank", p_rank);
	}
	
	// ***********查看等级N以上玩家************//
	public void getPlayerByPLevel(HttpServletRequest request, int level)
	{
		// 获取当前页数
		int currentpage = RequestUtil.GetParamInteger(request, "pageCurrent");
		if (currentpage == 0)
		{
			currentpage = 1;
		}
		// 获取数据的总条数
		int count = gmPlayerDao.getPlayerByPLevel(level).size();
		// 创建分页实体
		PageUtil page = new PageUtil(count, 10, currentpage);
		List<Player> list = gmPlayerDao.getPlayerByPLevelByPage(page.getCurrentPage(), page.getPageSize(), level);
		for (int i = 0; i < list.size(); i++)
		{
			Player player = list.get(i);
			PlayerInfo p = Cache.getInstance().getPlayerInfoForGm(player.getId());
			if (p != null)
			{
				list.remove(i);
				list.add(i, p.player);
			}
		}
		managerLogger.info(getManager(request).getName() + "|查看等级" + level + "以上的玩家" + "|登录IP：" + request.getRemoteAddr());
		String[] num = new String[page.getTotalPage()];
		request.setAttribute("page", page);
		request.setAttribute("num", num);
		request.setAttribute("list", list);
		request.setAttribute("level", level);
	}
	
	// ***********根据角色名称精确查询************//
	public void getPlayerByPname(HttpServletRequest request, String name)
	{
		try
		{
			// 获取当前页数
			int currentpage = RequestUtil.GetParamInteger(request, "pageCurrent");
			if (currentpage == 0)
			{
				currentpage = 1;
			}
			// 获取数据的总条数
			int count = gmPlayerDao.getPlayerByPName(name).size();
			// 创建分页实体
			PageUtil page = new PageUtil(count, 10, currentpage);
			// 数据库查名字匹配的,查出来一个集合
			List<Player> list = gmPlayerDao.getPlayerByPName(page.getCurrentPage(), page.getPageSize(), name);
			for (int i = 0; i < list.size(); i++)
			{
				player = list.get(i);
				// 优先从在线玩家获取
				PlayerInfo pInfo = Cache.getInstance().getPlayerInfoForGm(player.getId());
				if (pInfo != null)
				{
					list.remove(i);
					list.add(i, pInfo.player);
				}
			}
			String[] num = new String[page.getTotalPage()];
			// String href = "gm.htm?action=mhp_name&p_name=" + name +
			// "&pageCurrent=";
			request.setAttribute("page", page);
			request.setAttribute("num", num);
			// request.setAttribute("myherf", href);
			request.setAttribute("name", name);
			if (list.size() > 0 && list != null)
			{
				request.setAttribute("list", list);
			}
			else
			{
				request.setAttribute("msg", "没有找到相关角色!!");
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	// ***********vip经验N以上玩家************//
	public void getPlayerByVipCost(HttpServletRequest request, int vipCost)
	{
		try
		{
			// 获取当前页数
			int currentpage = RequestUtil.GetParamInteger(request, "pageCurrent");
			if (currentpage == 0)
			{
				currentpage = 1;
			}
			// 获取数据的总条数
			int count = gmPlayerDao.getPlayerByPvipCost(vipCost).size();
			// 创建分页实体
			PageUtil page = new PageUtil(count, 10, currentpage);
			// 数据库查名字匹配的,查出来一个集合
			List<Player> list = gmPlayerDao.getPlayerByPvipCost(page.getCurrentPage(), page.getPageSize(), vipCost);
			for (int i = 0; i < list.size(); i++)
			{
				player = list.get(i);
				// 优先从在线玩家获取
				PlayerInfo pInfo = Cache.getInstance().getPlayerInfoForGm(player.getId());
				if (pInfo != null)
				{
					list.remove(i);
					list.add(i, pInfo.player);
				}
			}
			String[] num = new String[page.getTotalPage()];
			// String href = "gm.htm?action=mhp_name&p_name=" + name +
			// "&pageCurrent=";
			request.setAttribute("page", page);
			request.setAttribute("num", num);
			// request.setAttribute("myherf", href);
			request.setAttribute("name", vipCost);
			if (list.size() > 0 && list != null)
			{
				request.setAttribute("list", list);
			}
			else
			{
				request.setAttribute("msg", "没有找到相关角色!!");
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/** **去发邮件** */
	public ModelAndView gomail(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("mail.jsp");
	}
	
	/** 发邮件* */
	public ModelAndView sendMail(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		try
		{
			Manager manager = (Manager) request.getSession().getAttribute(Constant.LOGIN_USER);
			// 发放方式:1全服发放,2批量发放
			int type = StringUtil.getInt(request.getParameter("type"));
			
			String title = request.getParameter("title");
			String receiver = request.getParameter("receiver");
			String content = request.getParameter("content");
			
			int rewardType1 = StringUtil.getInt(request.getParameter("rewardType1"));
			int rewardId1 = StringUtil.getInt(request.getParameter("rewardId1"));
			int rewardNumber1 = StringUtil.getInt(request.getParameter("rewardNumber1"));
			
			int rewardType2 = StringUtil.getInt(request.getParameter("rewardType2"));
			int rewardId2 = StringUtil.getInt(request.getParameter("rewardId2"));
			int rewardNumber2 = StringUtil.getInt(request.getParameter("rewardNumber2"));
			
			int rewardType3 = StringUtil.getInt(request.getParameter("rewardType3"));
			int rewardId3 = StringUtil.getInt(request.getParameter("rewardId3"));
			int rewardNumber3 = StringUtil.getInt(request.getParameter("rewardNumber3"));
			
			int rewardType4 = StringUtil.getInt(request.getParameter("rewardType4"));
			int rewardId4 = StringUtil.getInt(request.getParameter("rewardId4"));
			int rewardNumber4 = StringUtil.getInt(request.getParameter("rewardNumber4"));
			
			int rewardType5 = StringUtil.getInt(request.getParameter("rewardType5"));
			int rewardId5 = StringUtil.getInt(request.getParameter("rewardId5"));
			int rewardNumber5 = StringUtil.getInt(request.getParameter("rewardNumber5"));
			
			int rewardType6 = StringUtil.getInt(request.getParameter("rewardType6"));
			int rewardId6 = StringUtil.getInt(request.getParameter("rewardId6"));
			int rewardNumber6 = StringUtil.getInt(request.getParameter("rewardNumber6"));
			
			int gold = StringUtil.getInt(request.getParameter("gold"));
			int crystal = StringUtil.getInt(request.getParameter("crystal"));
			int runeNum = StringUtil.getInt(request.getParameter("runeNum"));
			int power = StringUtil.getInt(request.getParameter("power"));
			int friendNum = StringUtil.getInt(request.getParameter("friendNum"));
			if (rewardType1 == 0)
			{
				rewardType1 = 1;
			}
			if (rewardType2 == 0)
			{
				rewardType2 = 1;
			}
			if (rewardType3 == 0)
			{
				rewardType3 = 1;
			}
			if (rewardType4 == 0)
			{
				rewardType4 = 1;
			}
			if (rewardType5 == 0)
			{
				rewardType5 = 1;
			}
			if (rewardType6 == 0)
			{
				rewardType6 = 1;
			}
			String sender = "GM";
			String reward1 = rewardType1 + "&" + rewardId1 + "&" + rewardNumber1;
			String reward2 = rewardType2 + "&" + rewardId2 + "&" + rewardNumber2;
			String reward3 = rewardType3 + "&" + rewardId3 + "&" + rewardNumber3;
			String reward4 = rewardType4 + "&" + rewardId4 + "&" + rewardNumber4;
			String reward5 = rewardType5 + "&" + rewardId5 + "&" + rewardNumber5;
			String reward6 = rewardType6 + "&" + rewardId6 + "&" + rewardNumber6;
			
			int deleteDay = StringUtil.getInt(request.getParameter("deleteDay"));
			int deleteHour = StringUtil.getInt(request.getParameter("deleteHour"));
			int deleteMinute = StringUtil.getInt(request.getParameter("deleteMinute"));
			int deleteTime = deleteDay * 24 * 60 + deleteHour * 60 + deleteMinute;
			
			if (type == 1)
			{
				managerLogger.info("发送全服邮件开始");
				// 全服发(level>=4)
				List<Integer>[] playerIds = Cache.getInstance().getAllPlayerIds();
				// 在线
				for (int playerId : playerIds[0])
				{
					Mail mail = Mail.createMail(playerId, sender, title, content, reward1, reward2, reward3, reward4, reward5, reward6, gold, crystal, runeNum, power, friendNum, deleteTime);
					Cache.getInstance().sendMail(mail);
				}
				managerLogger.info(manager.getName() + "|邮件标题:" + title + "|在线玩家数：" + playerIds[0].size() + "|登录IP:" + request.getRemoteAddr());
				// 不在线
				List<Mail> mails = new ArrayList<Mail>();
				for (int playerId : playerIds[1])
				{
					Mail mail = Mail.createMail(playerId, sender, title, content, reward1, reward2, reward3, reward4, reward5, reward6, gold, crystal, runeNum, power, friendNum, deleteTime);
					mails.add(mail);
				}
				managerLogger.info(manager.getName() + "|邮件标题:" + title + "|不在线玩家数：" + playerIds[1].size() + "|登录IP:" + request.getRemoteAddr());
				Cache.getInstance().batchSaveMail(mails);
				request.setAttribute("msg", "success");
				managerLogger.info("发送全服邮件结束");
			}
			else if (type == 2)
			{
				managerLogger.info("批量发送邮件开始");
				// 批量发
				if (receiver == null || "".equals(receiver))
				{
					request.setAttribute("msg", "fail");
				}
				else
				{
					String[] ss = receiver.split(",");
					for (String s : ss)
					{
						int playerId = StringUtil.getInt(s);
						Mail mail = Mail.createMail(playerId, sender, title, content, reward1, reward2, reward3, reward4, reward5, reward6, gold, crystal, runeNum, power, friendNum, deleteTime);
						Cache.getInstance().sendMail(mail);
					}
					managerLogger.info(manager.getName() + "|发送批量邮件" + "|邮件标题:" + title + "|登录IP:" + request.getRemoteAddr());
					request.setAttribute("msg", "success");
				}
				
				managerLogger.info("批量邮件发送结束");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return new ModelAndView("mail.jsp");
	}
	
	public ModelAndView addTimeMail(HttpServletRequest request, HttpServletResponse response)
	{
		
		String title = request.getParameter("title");
		String content = request.getParameter("content");
		
		int rewardType1 = StringUtil.getInt(request.getParameter("rewardType1"));
		int rewardId1 = StringUtil.getInt(request.getParameter("rewardId1"));
		int rewardNumber1 = StringUtil.getInt(request.getParameter("rewardNumber1"));
		
		int rewardType2 = StringUtil.getInt(request.getParameter("rewardType2"));
		int rewardId2 = StringUtil.getInt(request.getParameter("rewardId2"));
		int rewardNumber2 = StringUtil.getInt(request.getParameter("rewardNumber2"));
		
		int rewardType3 = StringUtil.getInt(request.getParameter("rewardType3"));
		int rewardId3 = StringUtil.getInt(request.getParameter("rewardId3"));
		int rewardNumber3 = StringUtil.getInt(request.getParameter("rewardNumber3"));
		
		int rewardType4 = StringUtil.getInt(request.getParameter("rewardType4"));
		int rewardId4 = StringUtil.getInt(request.getParameter("rewardId4"));
		int rewardNumber4 = StringUtil.getInt(request.getParameter("rewardNumber4"));
		
		int rewardType5 = StringUtil.getInt(request.getParameter("rewardType5"));
		int rewardId5 = StringUtil.getInt(request.getParameter("rewardId5"));
		int rewardNumber5 = StringUtil.getInt(request.getParameter("rewardNumber5"));
		
		int rewardType6 = StringUtil.getInt(request.getParameter("rewardType6"));
		int rewardId6 = StringUtil.getInt(request.getParameter("rewardId6"));
		int rewardNumber6 = StringUtil.getInt(request.getParameter("rewardNumber6"));
		
		int gold = StringUtil.getInt(request.getParameter("gold"));
		int crystal = StringUtil.getInt(request.getParameter("crystal"));
		int runeNum = StringUtil.getInt(request.getParameter("runeNum"));
		int power = StringUtil.getInt(request.getParameter("power"));
		int friendNum = StringUtil.getInt(request.getParameter("friendNum"));
		
		if (!isValidMailAttachField(rewardType1, rewardId1, rewardNumber1))
		{
			request.setAttribute("msg", "设置失败,请检查第一个附带物品数据");
			return new ModelAndView("timemail/timemail.vm");
		}
		if (!isValidMailAttachField(rewardType2, rewardId2, rewardNumber2))
		{
			request.setAttribute("msg", "设置失败,请检查第二个附带物品数据");
			return new ModelAndView("timemail/timemail.vm");
		}
		if (!isValidMailAttachField(rewardType3, rewardId3, rewardNumber3))
		{
			request.setAttribute("msg", "设置失败,请检查第三个附带物品数据");
			return new ModelAndView("timemail/timemail.vm");
		}
		if (!isValidMailAttachField(rewardType4, rewardId4, rewardNumber4))
		{
			request.setAttribute("msg", "设置失败,请检查第四个附带物品数据");
			return new ModelAndView("timemail/timemail.vm");
		}
		if (!isValidMailAttachField(rewardType5, rewardId5, rewardNumber5))
		{
			request.setAttribute("msg", "设置失败,请检查第五个附带物品数据");
			return new ModelAndView("timemail/timemail.vm");
		}
		if (!isValidMailAttachField(rewardType6, rewardId6, rewardNumber6))
		{
			request.setAttribute("msg", "设置失败,请检查第六个附带物品数据");
			return new ModelAndView("timemail/timemail.vm");
		}
		// String sender = "GM";
		String reward1 = rewardType1 + "&" + rewardId1 + "&" + rewardNumber1;
		String reward2 = rewardType2 + "&" + rewardId2 + "&" + rewardNumber2;
		String reward3 = rewardType3 + "&" + rewardId3 + "&" + rewardNumber3;
		String reward4 = rewardType4 + "&" + rewardId4 + "&" + rewardNumber4;
		String reward5 = rewardType5 + "&" + rewardId5 + "&" + rewardNumber5;
		String reward6 = rewardType6 + "&" + rewardId6 + "&" + rewardNumber6;
		
		String startDay = request.getParameter("startDay");
		String endDay = request.getParameter("endDay");
		String hour = request.getParameter("hour");
		String minute = request.getParameter("minute");
		String sendTime = hour + ":" + minute;
		
		int deleteDay = StringUtil.getInt(request.getParameter("deleteDay"));
		int deleteHour = StringUtil.getInt(request.getParameter("deleteHour"));
		int deleteMinute = StringUtil.getInt(request.getParameter("deleteMinute"));
		int deleteTime = deleteDay * 24 * 60 + deleteHour * 60 + deleteMinute;
		TimeMail timeMail = TimeMail.createMail(title, content, reward1, reward2, reward3, reward4, reward5, reward6, gold, crystal, runeNum, power, friendNum, startDay, endDay, sendTime, deleteTime);
		
		timeMailDao.save(timeMail);
		managerLogger.info(getManager(request).getName() + "|添加了定时发送邮件" + "|邮件标题:" + timeMail.getTitle() + "|登录IP：" + request.getRemoteAddr());
		return goTimeMail(request, response);
	}
	
	/** **开启定时发送邮件** */
	public ModelAndView openTimeMail(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		int id = Integer.parseInt(request.getParameter("id"));
		TimeMail tm = timeMailDao.one(id);
		managerLogger.info(getManager(request).getName() + "|开启了定时发送邮件" + "|邮件标题：" + tm.getTitle() + "|登录IP：" + request.getRemoteAddr());
		boolean result = MailThread.getInstance().addTimeMail(tm);
		if (result)
		{
			request.setAttribute("msg", "已经开启");
			managerLogger.info("开启成功");
			tm.setState(1);
			logger.info("tm state:" + tm.getState() + "tm id：" + tm.getId());
			timeMailDao.update(tm);
		}
		else
		{
			request.setAttribute("msg", "开启失败,请检查数据是否正确");
			managerLogger.info("开启失败");
		}
		List<TimeMail> timeMails = timeMailDao.getAll();
		request.setAttribute("timeMail", timeMails);
		return new ModelAndView("timemail/all.vm");
	}
	
	/** *********移除定时发邮件线程中的某个任务****************** */
	public ModelAndView delThreadTimeMail(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		int id = Integer.parseInt(request.getParameter("id"));
		TimeMail tm = timeMailDao.one(id);
		managerLogger.info(getManager(request).getName() + "移除定时发送邮件线程中的邮件" + "|邮件标题：" + tm.getTitle() + "登录IP：" + request.getRemoteAddr());
		boolean result = MailThread.getInstance().delTimeMail(tm);
		if (result)
		{
			request.setAttribute("msg", "已经移除任务");
			managerLogger.info("移除成功");
			tm.setState(0);
			logger.info("tm state:" + tm.getState() + "tm id：" + tm.getId());
			timeMailDao.update(tm);
		}
		else
		{
			request.setAttribute("msg", "失败失败,请检查数据是否正确");
			managerLogger.info("移除失败");
		}
		List<TimeMail> timeMails = timeMailDao.getAll();
		request.setAttribute("timeMail", timeMails);
		return new ModelAndView("timemail/all.vm");
	}
	
	/** **去定时发邮件** */
	public ModelAndView goTimeMail(HttpServletRequest request, HttpServletResponse response)
	{
		List<TimeMail> timeMail = timeMailDao.getAll();
		request.setAttribute("timeMail", timeMail);
		return new ModelAndView("timemail/all.vm");
	}
	
	/** 去定制页面* */
	public ModelAndView goAddTimeMail(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		return new ModelAndView("/timemail/timemail.vm");
	}
	
	/** 去特殊发送邮件 * */
	public ModelAndView goSpecialMail(HttpServletRequest request, HttpServletResponse response)
	{
		List<SpeciaMail> list = commDao.getSpeciaMails();
		request.setAttribute("SpeciaMail", list);
		return new ModelAndView("specialmail/all.vm");
	}
	
	public ModelAndView goAddSpeciaMail(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("specialmail/specialmail.vm");
	}
	
	/** ***添加特殊邮件*** */
	public ModelAndView addSpeciaMail(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		// 发放方式:1完成n场竞技场,2获得n场竞技场胜利，3累计充值返利,4单笔充值nRMB发奖励,5累计消耗n钻石发奖励,6单笔消耗n钻石发奖励,7等级达到n级发奖励
		// 8战力达到n发奖励
		int type = StringUtil.getInt(request.getParameter("type"));
		
		String title = request.getParameter("title");
		String content = request.getParameter("content");
		
		int rewardType1 = StringUtil.getInt(request.getParameter("rewardType1"));
		int rewardId1 = StringUtil.getInt(request.getParameter("rewardId1"));
		int rewardNumber1 = StringUtil.getInt(request.getParameter("rewardNumber1"));
		
		int rewardType2 = StringUtil.getInt(request.getParameter("rewardType2"));
		int rewardId2 = StringUtil.getInt(request.getParameter("rewardId2"));
		int rewardNumber2 = StringUtil.getInt(request.getParameter("rewardNumber2"));
		
		int rewardType3 = StringUtil.getInt(request.getParameter("rewardType3"));
		int rewardId3 = StringUtil.getInt(request.getParameter("rewardId3"));
		int rewardNumber3 = StringUtil.getInt(request.getParameter("rewardNumber3"));
		
		int rewardType4 = StringUtil.getInt(request.getParameter("rewardType4"));
		int rewardId4 = StringUtil.getInt(request.getParameter("rewardId4"));
		int rewardNumber4 = StringUtil.getInt(request.getParameter("rewardNumber4"));
		
		int rewardType5 = StringUtil.getInt(request.getParameter("rewardType5"));
		int rewardId5 = StringUtil.getInt(request.getParameter("rewardId5"));
		int rewardNumber5 = StringUtil.getInt(request.getParameter("rewardNumber5"));
		
		int rewardType6 = StringUtil.getInt(request.getParameter("rewardType6"));
		int rewardId6 = StringUtil.getInt(request.getParameter("rewardId6"));
		int rewardNumber6 = StringUtil.getInt(request.getParameter("rewardNumber6"));
		
		int gold = StringUtil.getInt(request.getParameter("gold"));
		int crystal = StringUtil.getInt(request.getParameter("crystal"));
		int runeNum = StringUtil.getInt(request.getParameter("runeNum"));
		int power = StringUtil.getInt(request.getParameter("power"));
		int friendNum = StringUtil.getInt(request.getParameter("friendNum"));
		
		if (!isValidMailAttachField(rewardType1, rewardId1, rewardNumber1))
		{
			request.setAttribute("msg", "设置失败,请检查第一个附带物品数据");
			return new ModelAndView("specialmail/specialmail.vm");
		}
		if (!isValidMailAttachField(rewardType2, rewardId2, rewardNumber2))
		{
			request.setAttribute("msg", "设置失败,请检查第二个附带物品数据");
			return new ModelAndView("specialmail/specialmail.vm");
		}
		if (!isValidMailAttachField(rewardType3, rewardId3, rewardNumber3))
		{
			request.setAttribute("msg", "设置失败,请检查第三个附带物品数据");
			return new ModelAndView("specialmail/specialmail.vm");
		}
		if (!isValidMailAttachField(rewardType4, rewardId4, rewardNumber4))
		{
			request.setAttribute("msg", "设置失败,请检查第四个附带物品数据");
			return new ModelAndView("specialmail/specialmail.vm");
		}
		if (!isValidMailAttachField(rewardType5, rewardId5, rewardNumber5))
		{
			request.setAttribute("msg", "设置失败,请检查第五个附带物品数据");
			return new ModelAndView("specialmail/specialmail.vm");
		}
		if (!isValidMailAttachField(rewardType6, rewardId6, rewardNumber6))
		{
			request.setAttribute("msg", "设置失败,请检查第六个附带物品数据");
			return new ModelAndView("specialmail/specialmail.vm");
		}
		
		String reward1 = rewardType1 + "&" + rewardId1 + "&" + rewardNumber1;
		String reward2 = rewardType2 + "&" + rewardId2 + "&" + rewardNumber2;
		String reward3 = rewardType3 + "&" + rewardId3 + "&" + rewardNumber3;
		String reward4 = rewardType4 + "&" + rewardId4 + "&" + rewardNumber4;
		String reward5 = rewardType5 + "&" + rewardId5 + "&" + rewardNumber5;
		String reward6 = rewardType6 + "&" + rewardId6 + "&" + rewardNumber6;
		
		String typeInfo = request.getParameter("typeInfo");
		int value = 0;
		int value2 = 0;
		String hour = null;
		String minute = null;
		String sendTime = null;
		if (type == 1)
		{
			value = StringUtil.getInt(request.getParameter("v1"));
		}
		if (type == 2)
		{
			value = StringUtil.getInt(request.getParameter("v2"));
		}
		if (type == 3)
		{
			value = StringUtil.getInt(request.getParameter("v3"));
		}
		if (type == 4)
		{
			value = StringUtil.getInt(request.getParameter("v4"));
		}
		if (type == 5)
		{
			value = StringUtil.getInt(request.getParameter("v5"));
		}
		if (type == 6)
		{
			value = StringUtil.getInt(request.getParameter("v6"));
		}
		if (type == 7)
		{
			value = StringUtil.getInt(request.getParameter("v7"));
		}
		if (type == 8)
		{
			value = StringUtil.getInt(request.getParameter("v8"));
		}
		if (type == 9)
		{
			value = StringUtil.getInt(request.getParameter("v9"));
		}
		if (type == 10)
		{
			value = StringUtil.getInt(request.getParameter("v10"));
		}
		if (type == 11)
		{
			value = StringUtil.getInt(request.getParameter("v11"));
			value2 = StringUtil.getInt(request.getParameter("v112"));
			hour = request.getParameter("hour");
			minute = request.getParameter("minute");
			sendTime = hour + ":" + minute;
		}
		if (type == 12)
		{
			value = StringUtil.getInt(request.getParameter("v12"));
			hour = request.getParameter("v12hour");
			minute = request.getParameter("v12minute");
			sendTime = hour + ":" + minute;
		}
		String beginTime = request.getParameter("beginDay");
		String endTime = request.getParameter("endDay");
		if (beginTime.compareTo(endTime) > 0)
		{
			request.setAttribute("msg", "开始时间不能大于结束时间");
			return new ModelAndView("specialmail/specialmail.vm");
		}
		SpeciaMail speciaMail = SpeciaMail.createMail(type, typeInfo, value, value2, beginTime, endTime, title, content, reward1, reward2, reward3, reward4, reward5, reward6, gold, crystal, runeNum, power, friendNum, sendTime);
		commDao.saveSpeciaMail(speciaMail);
		managerLogger.info(getManager(request).getName() + "|添加特殊邮件" + "|邮件标题:" + speciaMail.getTitle() + "|登录IP：" + request.getRemoteAddr());
		return goSpecialMail(request, response);
	}
	
	/** 去添加特殊发送邮件条件* */
	public ModelAndView goAddMailSpecia(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("specialmail/mailspecial.vm");
	}
	
	/** 去修改定时发送邮件 */
	public ModelAndView oneTimeMail(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		int id = Integer.parseInt(request.getParameter("id"));
		TimeMail timeMail = timeMailDao.one(id);
		if (timeMail != null)
		{
			logger.info("timeMail.getReward1:" + timeMail.getReward1());
			String[] reward1 = timeMail.getReward1().split("&");
			String[] reward2 = timeMail.getReward2().split("&");
			String[] reward3 = timeMail.getReward3().split("&");
			String[] reward4 = timeMail.getReward4().split("&");
			String[] reward5 = timeMail.getReward5().split("&");
			String[] reward6 = timeMail.getReward6().split("&");
			String[] sendTime1 = timeMail.getSendTime().split(":");
			PublicMailView publicMailView = new PublicMailView(Integer.parseInt(reward1[0]), Integer.parseInt(reward1[1]), Integer.parseInt(reward1[2]), Integer.parseInt(reward2[0]), Integer.parseInt(reward2[1]), Integer.parseInt(reward2[2]), Integer.parseInt(reward3[0]), Integer.parseInt(reward3[1]), Integer.parseInt(reward3[2]), Integer.parseInt(reward4[0]), Integer.parseInt(reward4[1]), Integer.parseInt(reward4[2]), Integer.parseInt(reward5[0]), Integer.parseInt(reward5[1]), Integer.parseInt(reward5[2]), Integer.parseInt(reward6[0]), Integer.parseInt(reward6[1]), Integer.parseInt(reward6[2]), Integer.parseInt(sendTime1[0]), Integer.parseInt(sendTime1[1]));
			logger.info("publicMailView.hour:" + publicMailView.getHour());
			request.setAttribute("timeMail", timeMail);
			request.setAttribute("publicMailView", publicMailView);
		}
		else
		{
			logger.info("timeMail 为空！");
		}
		
		return new ModelAndView("timemail/update.vm");
	}
	
	/** 修改定时发送邮件* */
	public ModelAndView updateTimeMail(HttpServletRequest request, HttpServletResponse response)
	{
		int id = Integer.parseInt(request.getParameter("id"));
		
		String title = request.getParameter("title");
		String content = request.getParameter("content");
		
		int rewardType1 = StringUtil.getInt(request.getParameter("rewardType1"));
		int rewardId1 = StringUtil.getInt(request.getParameter("rewardId1"));
		int rewardNumber1 = StringUtil.getInt(request.getParameter("rewardNumber1"));
		
		int rewardType2 = StringUtil.getInt(request.getParameter("rewardType2"));
		int rewardId2 = StringUtil.getInt(request.getParameter("rewardId2"));
		int rewardNumber2 = StringUtil.getInt(request.getParameter("rewardNumber2"));
		
		int rewardType3 = StringUtil.getInt(request.getParameter("rewardType3"));
		int rewardId3 = StringUtil.getInt(request.getParameter("rewardId3"));
		int rewardNumber3 = StringUtil.getInt(request.getParameter("rewardNumber3"));
		
		int rewardType4 = StringUtil.getInt(request.getParameter("rewardType4"));
		int rewardId4 = StringUtil.getInt(request.getParameter("rewardId4"));
		int rewardNumber4 = StringUtil.getInt(request.getParameter("rewardNumber4"));
		
		int rewardType5 = StringUtil.getInt(request.getParameter("rewardType5"));
		int rewardId5 = StringUtil.getInt(request.getParameter("rewardId5"));
		int rewardNumber5 = StringUtil.getInt(request.getParameter("rewardNumber5"));
		
		int rewardType6 = StringUtil.getInt(request.getParameter("rewardType6"));
		int rewardId6 = StringUtil.getInt(request.getParameter("rewardId6"));
		int rewardNumber6 = StringUtil.getInt(request.getParameter("rewardNumber6"));
		
		int gold = StringUtil.getInt(request.getParameter("gold"));
		int crystal = StringUtil.getInt(request.getParameter("crystal"));
		int runeNum = StringUtil.getInt(request.getParameter("runeNum"));
		int power = StringUtil.getInt(request.getParameter("power"));
		int friendNum = StringUtil.getInt(request.getParameter("friendNum"));
		
		TimeMail timeMail1 = timeMailDao.one(id);
		
		logger.info("timeMail.getReward1:" + timeMail1.getReward1());
		String[] reward11 = timeMail1.getReward1().split("&");
		String[] reward12 = timeMail1.getReward2().split("&");
		String[] reward13 = timeMail1.getReward3().split("&");
		String[] reward14 = timeMail1.getReward4().split("&");
		String[] reward15 = timeMail1.getReward5().split("&");
		String[] reward16 = timeMail1.getReward6().split("&");
		String[] sendTime1 = timeMail1.getSendTime().split(":");
		PublicMailView publicMailView = new PublicMailView(Integer.parseInt(reward11[0]), Integer.parseInt(reward11[1]), Integer.parseInt(reward11[2]), Integer.parseInt(reward12[0]), Integer.parseInt(reward12[1]), Integer.parseInt(reward12[2]), Integer.parseInt(reward13[0]), Integer.parseInt(reward13[1]), Integer.parseInt(reward13[2]), Integer.parseInt(reward14[0]), Integer.parseInt(reward14[1]), Integer.parseInt(reward14[2]), Integer.parseInt(reward15[0]), Integer.parseInt(reward15[1]), Integer.parseInt(reward15[2]), Integer.parseInt(reward16[0]), Integer.parseInt(reward16[1]), Integer.parseInt(reward16[2]), Integer.parseInt(sendTime1[0]), Integer.parseInt(sendTime1[1]));
		logger.info("publicMailView.hour:" + publicMailView.getHour());
		request.setAttribute("timeMail", timeMail1);
		request.setAttribute("publicMailView", publicMailView);
		
		if (!isValidMailAttachField(rewardType1, rewardId1, rewardNumber1))
		{
			request.setAttribute("msg", "设置失败,请检查第一个附带物品数据");
			return new ModelAndView("timemail/update.vm");
		}
		if (!isValidMailAttachField(rewardType2, rewardId2, rewardNumber2))
		{
			request.setAttribute("msg", "设置失败,请检查第二个附带物品数据");
			return new ModelAndView("timemail/update.vm");
		}
		if (!isValidMailAttachField(rewardType3, rewardId3, rewardNumber3))
		{
			request.setAttribute("msg", "设置失败,请检查第三个附带物品数据");
			return new ModelAndView("timemail/update.vm");
		}
		if (!isValidMailAttachField(rewardType4, rewardId4, rewardNumber4))
		{
			request.setAttribute("msg", "设置失败,请检查第四个附带物品数据");
			return new ModelAndView("timemail/update.vm");
		}
		if (!isValidMailAttachField(rewardType5, rewardId5, rewardNumber5))
		{
			request.setAttribute("msg", "设置失败,请检查第五个附带物品数据");
			return new ModelAndView("timemail/update.vm");
		}
		if (!isValidMailAttachField(rewardType6, rewardId6, rewardNumber6))
		{
			request.setAttribute("msg", "设置失败,请检查第六个附带物品数据");
			return new ModelAndView("timemail/update.vm");
		}
		// String sender = "GM";
		String reward1 = rewardType1 + "&" + rewardId1 + "&" + rewardNumber1;
		String reward2 = rewardType2 + "&" + rewardId2 + "&" + rewardNumber2;
		String reward3 = rewardType3 + "&" + rewardId3 + "&" + rewardNumber3;
		String reward4 = rewardType4 + "&" + rewardId4 + "&" + rewardNumber4;
		String reward5 = rewardType5 + "&" + rewardId5 + "&" + rewardNumber5;
		String reward6 = rewardType6 + "&" + rewardId6 + "&" + rewardNumber6;
		
		String startDay = request.getParameter("startDay");
		String endDay = request.getParameter("endDay");
		String hour = request.getParameter("hour");
		String minute = request.getParameter("minute");
		String sendTime = hour + ":" + minute;
		
		int deleteDay = StringUtil.getInt(request.getParameter("deleteDay"));
		int deleteHour = StringUtil.getInt(request.getParameter("deleteHour"));
		int deleteMinute = StringUtil.getInt(request.getParameter("deleteMinute"));
		int deleteTime1 = deleteDay * 24 * 60 + deleteHour * 60 + deleteMinute;
		
		TimeMail timeMail = TimeMail.createMail(title, content, reward1, reward2, reward3, reward4, reward5, reward6, gold, crystal, runeNum, power, friendNum, startDay, endDay, sendTime, deleteTime1);
		timeMail.setId(id);
		
		timeMailDao.update(timeMail);
		managerLogger.info(getManager(request).getName() + "|修改定时发送邮件,邮件ID:" + timeMail.getId() + "|登录IP:" + request.getRemoteAddr());
		
		goTimeMail(request, response);
		return new ModelAndView("timemail/all.vm");
	}
	
	/** 删除定时发送邮件* */
	public ModelAndView deleteTimeMail(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		int id = Integer.parseInt(request.getParameter("id"));
		TimeMail timeMail = new TimeMail();
		timeMail.setId(id);
		timeMailDao.delete(timeMail);
		managerLogger.info(getManager(request).getName() + "|删除定时发送邮件" + "|定时邮件ID：" + id + "|登录IP：" + request.getRemoteAddr());
		goTimeMail(request, response);
		return new ModelAndView("timemail/all.vm");
	}
	
	public ModelAndView openOrCloseSpecialMail(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		int id = Integer.parseInt(request.getParameter("id"));
		SpeciaMail speciaMail = commDao.one(id);
		int state = speciaMail.getState();
		if (state == 1)
		{
			speciaMail.setState(0);
			MailThread.getInstance().delSpeciaMail(speciaMail);
			managerLogger.info(getManager(request).getName() + "|关闭特殊邮件" + "|邮件标题:" + speciaMail.getTitle() + "|登录IP：" + request.getRemoteAddr());
		}
		if (state == 0)
		{
			speciaMail.setState(1);
			MailThread.getInstance().addSpeciaMail(speciaMail);
			managerLogger.info(getManager(request).getName() + "|开启特殊邮件" + "|邮件标题:" + speciaMail.getTitle() + "|登录IP：" + request.getRemoteAddr());
		}
		commDao.updateSpecialMail(speciaMail);
		return goSpecialMail(request, response);
	}
	
	public ModelAndView deleteSpecialMail(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		int id = Integer.parseInt(request.getParameter("id"));
		SpeciaMail speciaMail = new SpeciaMail();
		speciaMail.setId(id);
		commDao.deleteSpeciaMail(speciaMail);
		managerLogger.info(getManager(request).getName() + "|删除特殊邮件的条件" + "|条件ID：" + id + "|登录IP：" + request.getRemoteAddr());
		return goSpecialMail(request, response);
	}
	
	public ModelAndView closeCahcePage(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("closeCachePage.jsp");
	}
	
	public ModelAndView closeCache(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		managerLogger.info("执行关服保存数据指令的GM账号:" + getManager(request).getName());
		String key = request.getParameter("key");
		if (!"tkpsBEGAM#%ERSHO~2TDO,)WNZSZN".equals(key))
		{
			managerLogger.info("关闭失败");
			request.setAttribute("result", "关服密钥错误，无法保存数据!");
			return new ModelAndView("user/result.vm");
		}
		Cache.getInstance().closeCache();
		managerLogger.info("关闭成功");
		request.setAttribute("result", "保存数据完成，请联系开发人员关闭服务器!");
		return new ModelAndView("user/result.vm");
	}
	
	/** 发邮件获取附件* */
	public ModelAndView rewardTypeData(HttpServletRequest request, HttpServletResponse response)
	{
		int rewardType = Integer.parseInt(request.getParameter("rewardType"));
		logger.info("rewardType:" + rewardType);
		response.setCharacterEncoding("utf-8");
		StringBuilder sBuilder = new StringBuilder();
		try
		{
			switch (rewardType)
			{
				case 1:// 材料
					List<ItemsData> itemsList = ItemsData.getAllItemsData();
					for (ItemsData itemsData : itemsList)
					{
						logger.info("itemsData" + itemsData.id);
						sBuilder.append("<option value='" + itemsData.id + "'>" + itemsData.id + "(" + itemsData.name + ")" + "</option>");
					}
					break;
				case 2:// 装备
					List<EquipData> equipList = EquipData.getAllEquipDatas();
					for (EquipData equipData : equipList)
					{
						sBuilder.append("<option value='" + equipData.index + "'>" + equipData.index + "(" + equipData.name + ")" + "</option>");
					}
					break;
				case 3:// 英雄卡
					List<CardData> cardList = CardData.getAllCardDatas();
					for (CardData cardData : cardList)
					{
						sBuilder.append("<option value='" + cardData.id + "'>" + cardData.id + "(" + cardData.name + ")" + "</option>");
					}
					break;
				case 4:// 主动技能
					List<SkillData> skillList = SkillData.getSkillDatas();
					for (SkillData skillData : skillList)
					{
						sBuilder.append("<option value='" + skillData.index + "'>" + skillData.index + "(" + skillData.name + ")" + "</option>");
					}
					break;
				case 5:// 被动技能
					List<PassiveSkillData> passiveSkillList = PassiveSkillData.getAllDatas();
					for (PassiveSkillData passiveSkillData : passiveSkillList)
					{
						sBuilder.append("<option value='" + passiveSkillData.index + "'>" + passiveSkillData.index + "(" + passiveSkillData.name + ")" + "</option>");
					}
					break;
			}
			
			response.getWriter().write(sBuilder.toString());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	/** 去设置倍率* */
	public ModelAndView goSetProbability(HttpServletRequest request, HttpServletResponse response)
	{
		HashMap<Integer, Float> eventDropMulMap = Cache.getEventDropMulMap();
		List<EventView> eventList = new ArrayList<EventView>();
		for (int id : eventDropMulMap.keySet())
		{
			EventData eventData = EventData.getEventData(id);
			EventView eventView = EventView.creatEventMulView(eventData.ID, eventData.name, eventDropMulMap.get(id));
			eventList.add(eventView);
		}
		request.setAttribute("expf", Cache.getExpMul());
		request.setAttribute("dropf", Cache.getMissionDropMul());
		request.setAttribute("mazef", Cache.getMazeDropMul());
		request.setAttribute("goldf", Cache.getGoldDropMul());
		request.setAttribute("eventfs", eventList);
		return new ModelAndView("probability/probability.vm");
	}
	
	/** 修改经验爆率和掉落爆率* */
	public ModelAndView setProbability(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		// 获取数据,完成修改
		int expfInput = StringUtil.getInt(request.getParameter("expf"));
		int dropfInput = StringUtil.getInt(request.getParameter("dropf"));
		int mazefInput = StringUtil.getInt(request.getParameter("mazef"));
		int goldfInput = StringUtil.getInt(request.getParameter("goldf"));
		
		// List<EventData> list = EventData.getEvents();
		// for (int i = 0; i < list.size(); i++)
		// {
		// EventData eventData = list.get(i);
		// Integer id = eventData.ID;
		// float eventfInput = StringUtil.getFloat(request.getParameter(id
		// .toString()));
		// Cache.setEventDropMulMap(id, eventfInput);
		// managerLogger.info(manager.getName() + "|设置活动副本掉落倍率："
		// + eventData.name + ":" + eventfInput + " |登录IP"
		// + request.getRemoteAddr());
		// }
		// HashMap<Integer, Float> eventDropMulMap = Cache.getEventDropMulMap();
		// List<EventView> eventList = new ArrayList<EventView>();
		// for (int id : eventDropMulMap.keySet())
		// {
		// EventData eventData = EventData.getEventData(id);
		// EventView eventView = EventView.creatEventMulView(eventData.ID,
		// eventData.name, eventDropMulMap.get(id));
		// eventList.add(eventView);
		// }
		Cache.setExpMul(expfInput);
		Cache.setMissionDropMul(dropfInput);
		Cache.setMazeDropMul(mazefInput);
		Cache.setGoldDropMul(goldfInput);
		managerLogger.info(getManager(request).getName() + "|修改经验倍率的值为：[" + expfInput + "]|修改推图掉落倍率的值为：[" + dropfInput + "]|修改迷宫掉落倍率值为：[" + mazefInput + "]|修改金币掉落倍率的值为：[" + goldfInput + "]|登录IP" + request.getRemoteAddr());
		request.setAttribute("expf", Cache.getExpMul());
		request.setAttribute("dropf", Cache.getMissionDropMul());
		request.setAttribute("mazef", Cache.getMazeDropMul());
		request.setAttribute("goldf", Cache.getGoldDropMul());
		// request.setAttribute("eventfs", eventList);
		return new ModelAndView("probability/probability.vm");
	}
	
	/** 统计在线玩家* */
	public ModelAndView getPlayerNum(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			int onlineNum = Cache.getInstance().getOnLinePlayer();
			response.setCharacterEncoding("utf-8");
			StringBuilder sBuilder = new StringBuilder();
			sBuilder.append("<font color='blue'>" + onlineNum + "</font>");
			response.getWriter().write(sBuilder.toString());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	/** 统计注册玩家* */
	public ModelAndView getUserNum(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			int userNum = gmPlayerDao.getUserNum();
			response.setCharacterEncoding("utf-8");
			StringBuilder sBuilder = new StringBuilder();
			sBuilder.append("<font color='blue'>" + userNum + "</font>");
			response.getWriter().write(sBuilder.toString());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	/** 游戏内公告 * */
	public ModelAndView showAnnounce(HttpServletRequest request, HttpServletResponse response)
	{
		request.setAttribute("announce", Cache.getInstance().getAnnounces());
		return new ModelAndView("announce/announce.vm");
	}
	
	// 发公告
	public ModelAndView addAnnounce(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		String context = request.getParameter("announce");
		int aid = StringUtil.getInt(request.getParameter("aid"));
		int frequency = StringUtil.getInt(request.getParameter("frequency"));
		int num = StringUtil.getInt(request.getParameter("num"));
		Announce announce = Announce.createAnnounce(aid, context, frequency, num, 2);
		boolean result = Cache.instance.addAnnounce(announce);
		if (result)
		{
			managerLogger.info(getManager(request).getName() + "|发布了游戏跑马灯公告!" + "|登录IP：" + request.getRemoteAddr());
		}
		else
		{
			request.setAttribute("msg", "此ID已经存在，请重新输入!");
			return showAnnounce(request, response);
		}
		request.setAttribute("announce", Cache.getInstance().getAnnounces());
		return new ModelAndView("announce/announce.vm");
	}
	
	/**
	 * 修改公告
	 * 
	 * @throws UnsupportedEncodingException
	 */
	public ModelAndView upAnnounce(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException
	{
		String context = request.getParameter("announce");
		String str = java.net.URLDecoder.decode(context, "UTF-8");
		int frequency = StringUtil.getInt(request.getParameter("frequency"));
		int num = StringUtil.getInt(request.getParameter("num"));
		String qstr = request.getParameter("qstr");
		String[] sid = qstr.split("-");
		for (int i = 0; i < sid.length; i++)
		{
			int id = StringUtil.getInt(sid[i]);
			Announce announce = Announce.createAnnounce(id, str, frequency, num, 2);
			Cache.getInstance().upAnnounce(announce);
		}
		return showAnnounce(request, response);
	}
	
	/**
	 * 删除公告
	 * 
	 * @throws UnsupportedEncodingException
	 */
	public ModelAndView delAnnounce(HttpServletRequest request, HttpServletResponse response)
	{
		String sidr = request.getParameter("qstr");
		String[] sid = sidr.split("-");
		for (int i = 0; i < sid.length; i++)
		{
			int id = StringUtil.getInt(sid[i]);
			Cache.getInstance().delAnnounce(id);
		}
		return showAnnounce(request, response);
	}
	
	/** 去活动配置* */
	public ModelAndView goGmEvent(HttpServletRequest request, HttpServletResponse response)
	{
		int activityId = StringUtil.getInt(request.getParameter("activityId"));
		Activity activity = activityInfoDao.getActivity(activityId);
		List<ActivityInfo> list = activityInfoDao.getActivityInfoByActivityId(activityId);
		ActivityInfo activityInfo = null;
		if (list != null && list.size() > 0)
		{
			activityInfo = list.get(0);
			
		}
		else
		{
			activityInfo = new ActivityInfo();
		}
		request.setAttribute("activity", activity);
		request.setAttribute("activityInfo", activityInfo);
		if (activity.getType() == 7)
		{
			return new ModelAndView("gmevent/addActivityg.vm");
		}
		if (activity.getType() == 4)
		{
			return new ModelAndView("gmevent/addActivityd.vm");
		}
		return null;
	}
	
	/** 配置活动* */
	public ModelAndView addGmEvent(HttpServletRequest request, HttpServletResponse response)
	{
		int eventType = Integer.parseInt(request.getParameter("eventType"));
		int activityId = Integer.parseInt(request.getParameter("activityId"));
		List<ActivityInfo> list = activityInfoDao.getActivityInfoByActivityId(activityId);
		// ---- 公告型活动 -----//
		if (eventType == 7)
		{
			if (list.size() > 0 && list != null)
			{}
			String gName = request.getParameter("gName");
			String gContent = request.getParameter("gContent");
			int hot = StringUtil.getInt(request.getParameter("hot"));
			int weight = StringUtil.getInt(request.getParameter("weight"));
			ActivityInfo activityInfo = ActivityInfo.createActivityInfo(eventType, activityId, gName, gContent, hot, weight);
			Cache.getInstance().addActivityInfo(activityInfo);
			activityInfoDao.add(activityInfo);
			managerLogger.info(getManager(request).getName() + "|配置了公告展示型活动" + "|活动ID:" + activityId + "|活动名称:" + gName + "|登录ID：" + request.getRemoteAddr());
		}
		if (eventType == 4)// 兑换型
		{
			String eName = null;
			String eContent = null;
			if (list.size() > 0 && list != null)
			{
				ActivityInfo aInfo = list.get(0);
				eName = aInfo.getName();
				eContent = aInfo.getContent();
			}
			else
			{
				eName = request.getParameter("eName");
				eContent = request.getParameter("eContent");
			}
			int exchangeType = Integer.parseInt(request.getParameter("exchangeType1"));// 兑换物品类型ID
			int exchangeIdA = Integer.parseInt(request.getParameter("exchangeIdA1"));// 材料id
			int exchangeId = 0;
			int exchangeNum = 0;
			String exchangeTypeString = "";
			switch (exchangeType)
			{
				case 1:
					exchangeTypeString = "材料";
					exchangeId = exchangeIdA;
					exchangeNum = Integer.parseInt(request.getParameter("exchangeNumA1"));
					break;
				case 2:
					exchangeTypeString = "装备";
					exchangeId = exchangeIdA;
					exchangeNum = Integer.parseInt(request.getParameter("exchangeNumA1"));
					break;
				case 3:
					exchangeTypeString = "英雄卡";
					exchangeId = exchangeIdA;
					exchangeNum = Integer.parseInt(request.getParameter("exchangeNumA1"));
					break;
				case 4:
					exchangeTypeString = "主动技能";
					exchangeId = exchangeIdA;
					exchangeNum = Integer.parseInt(request.getParameter("exchangeNumA1"));
					break;
				case 5:
					exchangeTypeString = "被动技能";
					exchangeId = exchangeIdA;
					exchangeNum = Integer.parseInt(request.getParameter("exchangeNumA1"));
					break;
				case 6:
					exchangeTypeString = "金币";
					exchangeNum = Integer.parseInt(request.getParameter("exchangeNumB1"));
					break;
				case 8:
					exchangeTypeString = "钻石";
					exchangeNum = Integer.parseInt(request.getParameter("exchangeNumB1"));
					break;
				case 9:
					exchangeTypeString = "符文";
					exchangeNum = Integer.parseInt(request.getParameter("exchangeNumB1"));
					break;
				case 10:
					exchangeTypeString = "体力";
					exchangeNum = Integer.parseInt(request.getParameter("exchangeNumB1"));
					break;
			}
			int needType = Integer.parseInt(request.getParameter("needType1"));
			int needIdA = Integer.parseInt(request.getParameter("needIdA1"));
			int needId = 0;
			int needNum = 0;
			String needTypeString = "";
			switch (needType)
			{
				case 1:
					needTypeString = "材料";
					needId = needIdA;
					needNum = Integer.parseInt(request.getParameter("needNumA1"));
					break;
				case 2:
					needTypeString = "装备";
					needId = needIdA;
					needNum = Integer.parseInt(request.getParameter("needNumA1"));
					break;
				case 3:
					needTypeString = "英雄卡";
					needId = needIdA;
					needNum = Integer.parseInt(request.getParameter("needNumA1"));
					break;
				case 4:
					needTypeString = "主动技能";
					needId = needIdA;
					needNum = Integer.parseInt(request.getParameter("needNumA1"));
					break;
				case 5:
					needTypeString = "被动技能";
					needId = needIdA;
					needNum = Integer.parseInt(request.getParameter("needNumA1"));
					break;
				case 6:
					needTypeString = "金币";
					needNum = Integer.parseInt(request.getParameter("needNumB1"));
					break;
				case 8:
					needTypeString = "钻石";
					needNum = Integer.parseInt(request.getParameter("needNumB1"));
					break;
				case 9:
					needTypeString = "符文";
					needNum = Integer.parseInt(request.getParameter("needNumB1"));
					break;
				case 10:
					needTypeString = "体力";
					needNum = Integer.parseInt(request.getParameter("needNumB1"));
					break;
			}
			int needType2 = Integer.parseInt(request.getParameter("needType2"));
			int needIdA2 = Integer.parseInt(request.getParameter("needIdA2"));
			int needId2 = 0;
			int needNum2 = 0;
			String needTypeString2 = "";
			switch (needType2)
			{
				case 1:
					needTypeString2 = "材料";
					needId2 = needIdA2;
					needNum2 = Integer.parseInt(request.getParameter("needNumA2"));
					break;
				case 2:
					needTypeString2 = "装备";
					needId2 = needIdA2;
					needNum2 = Integer.parseInt(request.getParameter("needNumA2"));
					break;
				case 3:
					needTypeString2 = "英雄卡";
					needId2 = needIdA2;
					needNum2 = Integer.parseInt(request.getParameter("needNumA2"));
					break;
				case 4:
					needTypeString2 = "主动技能";
					needId2 = needIdA2;
					needNum2 = Integer.parseInt(request.getParameter("needNumA2"));
					break;
				case 5:
					needTypeString2 = "被动技能";
					needId2 = needIdA2;
					needNum2 = Integer.parseInt(request.getParameter("needNumA2"));
					break;
				case 6:
					needTypeString2 = "金币";
					needNum2 = Integer.parseInt(request.getParameter("needNumB2"));
					break;
				case 8:
					needTypeString2 = "钻石";
					needNum2 = Integer.parseInt(request.getParameter("needNumB2"));
					break;
				case 9:
					needTypeString2 = "符文";
					needNum2 = Integer.parseInt(request.getParameter("needNumB2"));
					break;
				case 10:
					needTypeString2 = "体力";
					needNum2 = Integer.parseInt(request.getParameter("needNumB2"));
					break;
			}
			int needType3 = Integer.parseInt(request.getParameter("needType3"));
			int needIdA3 = Integer.parseInt(request.getParameter("needIdA3"));
			int needId3 = 0;
			int needNum3 = 0;
			String needTypeString3 = "";
			switch (needType3)
			{
				case 1:
					needTypeString3 = "材料";
					needId3 = needIdA3;
					needNum3 = Integer.parseInt(request.getParameter("needNumA3"));
					break;
				case 2:
					needTypeString3 = "装备";
					needId3 = needIdA3;
					needNum3 = Integer.parseInt(request.getParameter("needNumA3"));
					break;
				case 3:
					needTypeString3 = "英雄卡";
					needId3 = needIdA3;
					needNum3 = Integer.parseInt(request.getParameter("needNumA3"));
					break;
				case 4:
					needTypeString3 = "主动技能";
					needId3 = needIdA3;
					needNum3 = Integer.parseInt(request.getParameter("needNumA3"));
					break;
				case 5:
					needTypeString3 = "被动技能";
					needId3 = needIdA3;
					needNum3 = Integer.parseInt(request.getParameter("needNumA3"));
					break;
				case 6:
					needTypeString3 = "金币";
					needNum3 = Integer.parseInt(request.getParameter("needNumB3"));
					break;
				case 8:
					needTypeString3 = "钻石";
					needNum3 = Integer.parseInt(request.getParameter("needNumB3"));
					break;
				case 9:
					needTypeString3 = "符文";
					needNum3 = Integer.parseInt(request.getParameter("needNumB3"));
					break;
				case 10:
					needTypeString3 = "体力";
					needNum3 = Integer.parseInt(request.getParameter("needNumB3"));
					break;
			}
			int sole = Integer.parseInt(request.getParameter("sole"));
			String exchangeContext = request.getParameter("exchangeContext");
			ActivityInfo activityInfo = ActivityInfo.createActivityInfo(eventType, activityId, eName, eContent, exchangeType, exchangeId, exchangeNum, needType, needId, needNum, needType2, needId2, needNum2, needType3, needId3, needNum3, sole, exchangeContext);
			Cache.getInstance().addActivityInfo(activityInfo);
			activityInfoDao.add(activityInfo);
			managerLogger.info(getManager(request).getName() + "|配置了物品兑换型活动:" + needNum + "个" + needTypeString + "(" + needId + ")" + "和" + needNum2 + "个" + needTypeString2 + "(" + needId2 + ")" + "和" + needNum3 + "个" + needTypeString3 + "(" + needId3 + ")" + "兑换--->" + exchangeNum + "个" + exchangeTypeString + "(" + exchangeId + ")" + "|活动ID:" + activityInfo.getId() + "|登录ID：" + request.getRemoteAddr());
		}
		request.setAttribute("msg", "success");
		return activityInfo(request, response);
	}
	
	public ModelAndView activity(HttpServletRequest request, HttpServletResponse response)
	{
		List<Activity> list = activityInfoDao.find();
		request.setAttribute("list", list);
		return new ModelAndView("gmevent/activity.vm");
	}
	
	public ModelAndView goAddActivity(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		return new ModelAndView("gmevent/addActivity.vm");
	}
	
	public ModelAndView addActivity(HttpServletRequest request, HttpServletResponse response)
	{
		int activityId = StringUtil.getInt(request.getParameter("activityId"));
		String activityName = request.getParameter("activityName");
		int weight = StringUtil.getInt(request.getParameter("weight"));
		int type = StringUtil.getInt(request.getParameter("type"));
		if (activityInfoDao.findByActivityId(activityId) > 0)
		{
			request.setAttribute("msg", "输入的ID已经存在");
			return new ModelAndView("gmevent/addActivity.vm");
		}
		Activity activity = Activity.createActivity(activityId, activityName, weight, type);
		Cache.getInstance().addActivity(activity);
		activityInfoDao.addActivity(activity);
		managerLogger.info(getManager(request).getName() + "|添加了新活动" + "|活动ID:" + activityId + "|登录ID：" + request.getRemoteAddr());
		return activity(request, response);
	}
	
	public ModelAndView skipUpActivity(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		int activityId = StringUtil.getInt(request.getParameter("activityId"));
		Activity activity = activityInfoDao.getActivity(activityId);
		request.setAttribute("activity", activity);
		return new ModelAndView("gmevent/upActivity.vm");
	}
	
	public ModelAndView upActivity(HttpServletRequest request, HttpServletResponse response)
	{
		int id = StringUtil.getInt(request.getParameter("id"));
		int activityId = StringUtil.getInt(request.getParameter("activityId"));
		String activityName = request.getParameter("activityName");
		int weight = StringUtil.getInt(request.getParameter("weight"));
		int type = StringUtil.getInt(request.getParameter("type"));
		Activity activity = Activity.createActivity(activityId, activityName, weight, type);
		activity.setId(id);
		activityInfoDao.upActivity(activity);
		Cache.getInstance().upActivity(activity);
		managerLogger.info(getManager(request).getName() + "|修改了活动" + "|活动ID:" + activityId + "|登录ID：" + request.getRemoteAddr());
		return activity(request, response);
	}
	
	public ModelAndView delActivity(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		int activityId = StringUtil.getInt(request.getParameter("activityId"));
		Cache.getInstance().delActivity(activityId);
		activityInfoDao.delActivity(activityId);
		managerLogger.info(getManager(request).getName() + "|删除了活动" + "|活动ID:" + activityId + "|登录ID：" + request.getRemoteAddr());
		return activity(request, response);
	}
	
	public ModelAndView activityInfo(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		int activityId = StringUtil.getInt(request.getParameter("activityId"));
		List<ActivityInfo> list = activityInfoDao.getActivityInfoByActivityId(activityId);
		ActivityInfo aInfo = null;
		if (list != null && list.size() > 0)
		{
			aInfo = list.get(0);
			
		}
		else
		{
			aInfo = new ActivityInfo();
			aInfo.setActivityId(activityId);
		}
		Activity activity = activityInfoDao.getActivity(activityId);
		int type = activity.getType();
		request.setAttribute("aInfo", aInfo);
		request.setAttribute("activityId", activityId);
		request.setAttribute("type", type);
		request.setAttribute("list", list);
		if (type == 7)
		{
			return new ModelAndView("gmevent/activityg.vm");
		}
		if (type == 4)
		{
			return new ModelAndView("gmevent/activityd.vm");
		}
		return null;
	}
	
	public ModelAndView delActivityInfo(HttpServletRequest request, HttpServletResponse response)
	{
		int id = StringUtil.getInt(request.getParameter("id"));
		ActivityInfo ai = Cache.getInstance().getActivityInfoByid(id);
		if (ai != null)
		{
			Cache.getInstance().delActivityInfo(id);
			activityInfoDao.delActivityInfo(id);
			String needTypeString = setNeedTypeString(ai.getNeedType());
			String needTypeString2 = setNeedTypeString(ai.getNeedType2());
			String needTypeString3 = setNeedTypeString(ai.getNeedType3());
			String exchangeTypeString = setNeedTypeString(ai.getExchangeType());
			managerLogger.info(getManager(request).getName() + "|删除了物品兑换型活动:" + ai.getNeedNum() + "个" + needTypeString + "(" + ai.getNeedId() + ")" + "和" + ai.getNeedNum2() + "个" + needTypeString2 + "(" + ai.getNeedId2() + ")" + "和" + ai.getNeedNum3() + "个" + needTypeString3 + "(" + ai.getNeedId3() + ")" + "兑换--->" + ai.getExchangeNum() + "个" + exchangeTypeString + "(" + ai.getExchangeId() + ")" + "|活动ID:" + ai.getId() + "|登录ID：" + request.getRemoteAddr());
		}
		return activityInfo(request, response);
	}
	
	private String setNeedTypeString(int needType)
	{
		String needTypeString = "";
		switch (needType)
		{
			case 1:
				needTypeString = "材料";
				break;
			case 2:
				needTypeString = "装备";
				break;
			case 3:
				needTypeString = "英雄卡";
				break;
			case 4:
				needTypeString = "主动技能";
				break;
			case 5:
				needTypeString = "被动技能";
				break;
			case 6:
				needTypeString = "金币";
				break;
			case 8:
				needTypeString = "钻石";
				break;
			case 9:
				needTypeString = "符文";
				break;
			case 10:
				needTypeString = "体力";
				break;
		}
		return needTypeString;
	}
	
	// 导出信息
	public ModelAndView exportInfo(HttpServletRequest request, HttpServletResponse response)
	{
		int id = StringUtil.getInt(request.getParameter("id"));
		List<PkRank> pklist = null;
		List<Player> plist = null;
		String fileName = "";
		if (id <= 0)
		{
			return null;
		}
		if (id == 1)
		{
			int p_rank = StringUtil.getInt(request.getParameter("p_rank"));
			if (p_rank <= 0)
			{
				return null;
			}
			HashMap<Integer, PkRank> pkMap = Cache.getInstance().getPkRankMap2();
			pklist = new ArrayList<PkRank>();
			for (int k = 1; k <= p_rank; k++)
			{
				PkRank pkRank = pkMap.get(k);
				if (pkRank != null)
				{
					pklist.add(pkRank);
				}
			}
			fileName = "pkRanks";
			managerLogger.info(getManager(request).getName() + "|导出竞技场排行" + p_rank + "以上的玩家" + "|登录IP：" + request.getRemoteAddr());
		}
		if (id == 2)
		{
			int level = StringUtil.getInt(request.getParameter("p_level"));
			if (level <= 0)
			{
				return null;
			}
			plist = gmPlayerDao.getPlayerByPLevel(level);
			for (int i = 0; i < plist.size(); i++)
			{
				Player player = plist.get(i);
				PlayerInfo p = Cache.getInstance().getPlayerInfoForGm(player.getId());
				if (p != null)
				{
					plist.remove(i);
					plist.add(i, p.player);
				}
			}
			fileName = "playerLevels";
			managerLogger.info(getManager(request).getName() + "|导出等级" + level + "以上的玩家" + "|登录IP：" + request.getRemoteAddr());
		}
		byte[] bs = getTxtBytes(id, plist, pklist);
		try
		{
			downLoad(request, response, fileName, bs);
		}
		catch (Exception e)
		{
			logger.error(e);
		}
		return null;
	}
	
	private void downLoad(HttpServletRequest request, HttpServletResponse response, String fileName, byte[] bs) throws Exception
	{
		// 设置文件的类型
		logger.debug("开始下载player文件");
		response.addHeader("Content-Disposition", "attachment;filename=" + fileName + ".txt");
		response.addHeader("Content-Length", "" + bs.length);
		OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
		response.setContentType("txt/html");
		toClient.write(bs);
		toClient.flush();
		toClient.close();
	}
	
	private static byte[] getTxtBytes(int id, List<Player> plist, List<PkRank> pklist)
	{
		if (id == 2)
		{
			StringBuffer sb = new StringBuffer();
			sb.append("玩家ID\t玩家名称\t玩家等级\n");
			for (Player player : plist)
			{
				sb.append(player.getId() + "\t" + player.getName() + "\t" + player.getLevel() + "\n");
			}
			return sb.toString().getBytes();
		}
		else
		{
			StringBuffer sb = new StringBuffer();
			sb.append("玩家ID\t玩家等级\t玩家排行\n");
			for (PkRank pkRank : pklist)
			{
				sb.append(pkRank.getPlayerId() + "\t" + pkRank.getPlayerLevel() + "\t" + pkRank.getRank() + "\n");
			}
			return sb.toString().getBytes();
		}
	}
	
	public ModelAndView goUpActivityInfod(HttpServletRequest request, HttpServletResponse response)
	{
		int id = Integer.parseInt(request.getParameter("id"));
		ActivityInfo activityInfo = Cache.getInstance().getActivityInfoByid(id);
		request.setAttribute("activityInfo", activityInfo);
		int activityId = StringUtil.getInt(request.getParameter("activityId"));
		Activity activity = activityInfoDao.getActivity(activityId);
		request.setAttribute("activity", activity);
		return new ModelAndView("gmevent/upActivityd.vm");
	}
	
	public ModelAndView goUpActivityInfog(HttpServletRequest request, HttpServletResponse response)
	{
		int id = Integer.parseInt(request.getParameter("id"));
		ActivityInfo activityInfo = Cache.getInstance().getActivityInfoByid(id);
		request.setAttribute("activityInfo", activityInfo);
		int activityId = StringUtil.getInt(request.getParameter("activityId"));
		Activity activity = activityInfoDao.getActivity(activityId);
		request.setAttribute("activity", activity);
		return new ModelAndView("gmevent/upActivityg.vm");
	}
	
	public ModelAndView upActivityg(HttpServletRequest request, HttpServletResponse response)
	{
		int id = Integer.parseInt(request.getParameter("id"));
		int activityId = StringUtil.getInt(request.getParameter("activityId"));
		String gName = request.getParameter("gName");
		String gContent = request.getParameter("gContent");
		int hot = StringUtil.getInt(request.getParameter("hot"));
		int eventType = StringUtil.getInt(request.getParameter("eventType"));
		int weight = StringUtil.getInt(request.getParameter("weight"));
		ActivityInfo activityInfo = ActivityInfo.createActivityInfo(eventType, activityId, gName, gContent, hot, weight);
		activityInfo.setId(id);
		Cache.getInstance().upActivityInfo(activityInfo);
		activityInfoDao.upActivityInfo(activityInfo);
		managerLogger.info(getManager(request).getName() + "|修改了公告展示型活动内容" + "|所属活动ID:" + activityId + "|登录ID：" + request.getRemoteAddr());
		return activityInfo(request, response);
	}
	
	public ModelAndView upActivityd(HttpServletRequest request, HttpServletResponse response)
	{
		int id = Integer.parseInt(request.getParameter("id"));
		int eventType = Integer.parseInt(request.getParameter("eventType"));
		int activityId = Integer.parseInt(request.getParameter("activityId"));
		List<ActivityInfo> list = activityInfoDao.getActivityInfoByActivityId(activityId);
		String eName = null;
		String eContent = null;
		if (list.size() > 0 && list != null)
		{
			ActivityInfo aInfo = list.get(0);
			eName = aInfo.getName();
			eContent = aInfo.getContent();
		}
		else
		{
			eName = request.getParameter("eName");
			eContent = request.getParameter("eContent");
		}
		int exchangeType = Integer.parseInt(request.getParameter("exchangeType1"));// 兑换物品类型ID
		int exchangeIdA = Integer.parseInt(request.getParameter("exchangeIdA1"));// 材料id
		// or
		// 卡牌id
		// or
		int exchangeId = 0;
		int exchangeNum = 0;
		switch (exchangeType)
		{
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
				exchangeId = exchangeIdA;
				exchangeNum = Integer.parseInt(request.getParameter("exchangeNumA1"));
				break;
			case 6:
			case 8:
			case 9:
			case 10:
				exchangeNum = Integer.parseInt(request.getParameter("exchangeNumB1"));
				break;
		}
		int needType = Integer.parseInt(request.getParameter("needType1"));
		int needIdA = Integer.parseInt(request.getParameter("needIdA1"));
		int needId = 0;
		int needNum = 0;
		switch (needType)
		{
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
				needId = needIdA;
				needNum = Integer.parseInt(request.getParameter("needNumA1"));
				break;
			case 6:
			case 8:
			case 9:
			case 10:
				needNum = Integer.parseInt(request.getParameter("needNumB1"));
				break;
		}
		int needType2 = Integer.parseInt(request.getParameter("needType2"));
		int needIdA2 = Integer.parseInt(request.getParameter("needIdA2"));
		int needId2 = 0;
		int needNum2 = 0;
		switch (needType2)
		{
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
				needId2 = needIdA2;
				needNum2 = Integer.parseInt(request.getParameter("needNumA2"));
				break;
			case 6:
			case 8:
			case 9:
			case 10:
				needNum2 = Integer.parseInt(request.getParameter("needNumB2"));
				break;
		}
		int needType3 = Integer.parseInt(request.getParameter("needType3"));
		int needIdA3 = Integer.parseInt(request.getParameter("needIdA3"));
		int needId3 = 0;
		int needNum3 = 0;
		switch (needType3)
		{
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
				needId3 = needIdA3;
				needNum3 = Integer.parseInt(request.getParameter("needNumA3"));
				break;
			case 6:
			case 8:
			case 9:
			case 10:
				needNum3 = Integer.parseInt(request.getParameter("needNumB3"));
				break;
		}
		int sole = Integer.parseInt(request.getParameter("sole"));
		String exchangeContext = request.getParameter("exchangeContext");
		ActivityInfo activityInfo = ActivityInfo.createActivityInfo(eventType, activityId, eName, eContent, exchangeType, exchangeId, exchangeNum, needType, needId, needNum, needType2, needId2, needNum2, needType3, needId3, needNum3, sole, exchangeContext);
		activityInfo.setId(id);
		Cache.getInstance().upActivityInfo(activityInfo);
		activityInfoDao.upActivityInfo(activityInfo);
		managerLogger.info(getManager(request).getName() + "|修改了物品兑换型活动内容" + "|活动ID:" + id + "|登录ID：" + request.getRemoteAddr());
		request.setAttribute("msg", "success");
		return activityInfo(request, response);
	}
	
	public ModelAndView friendPower(HttpServletRequest request, HttpServletResponse response)
	{
		request.setAttribute("sendTime", Cache.maxPowerTimes);
		request.setAttribute("sendPower", Cache.friendPower);
		return new ModelAndView("friendPower.jsp");
	}
	
	public ModelAndView modFriendPower(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		String temp = request.getParameter("sendTime");
		String temp2 = request.getParameter("sendPower");
		int sendTime = StringUtil.getInt(temp);
		int sendPower = StringUtil.getInt(temp2);
		if (sendTime > 0 && sendPower > 0)
		{
			Cache.maxPowerTimes = sendTime;
			Cache.friendPower = sendPower;
			managerLogger.info(getManager(request).getName() + "|把接收好友体力上限改为" + sendTime + ",把每次接收体力值改为" + sendPower + "|登录IP：" + request.getRemoteAddr());
		}
		request.setAttribute("msg", "修改成功");
		request.setAttribute("sendTime", Cache.maxPowerTimes);
		request.setAttribute("sendPower", Cache.friendPower);
		return new ModelAndView("friendPower.jsp");
	}
	
	public ModelAndView goAddZcMail(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		return new ModelAndView("zcmail/zcmail.vm");
	}
	
	public ModelAndView addZcMail(HttpServletRequest request, HttpServletResponse response)
	{
		String title = request.getParameter("title");
		String content = request.getParameter("content");
		
		int rewardType1 = StringUtil.getInt(request.getParameter("rewardType1"));
		int rewardId1 = StringUtil.getInt(request.getParameter("rewardId1"));
		int rewardNumber1 = StringUtil.getInt(request.getParameter("rewardNumber1"));
		
		int rewardType2 = StringUtil.getInt(request.getParameter("rewardType2"));
		int rewardId2 = StringUtil.getInt(request.getParameter("rewardId2"));
		int rewardNumber2 = StringUtil.getInt(request.getParameter("rewardNumber2"));
		
		int rewardType3 = StringUtil.getInt(request.getParameter("rewardType3"));
		int rewardId3 = StringUtil.getInt(request.getParameter("rewardId3"));
		int rewardNumber3 = StringUtil.getInt(request.getParameter("rewardNumber3"));
		
		int rewardType4 = StringUtil.getInt(request.getParameter("rewardType4"));
		int rewardId4 = StringUtil.getInt(request.getParameter("rewardId4"));
		int rewardNumber4 = StringUtil.getInt(request.getParameter("rewardNumber4"));
		
		int rewardType5 = StringUtil.getInt(request.getParameter("rewardType5"));
		int rewardId5 = StringUtil.getInt(request.getParameter("rewardId5"));
		int rewardNumber5 = StringUtil.getInt(request.getParameter("rewardNumber5"));
		
		int rewardType6 = StringUtil.getInt(request.getParameter("rewardType6"));
		int rewardId6 = StringUtil.getInt(request.getParameter("rewardId6"));
		int rewardNumber6 = StringUtil.getInt(request.getParameter("rewardNumber6"));
		
		int gold = StringUtil.getInt(request.getParameter("gold"));
		int crystal = StringUtil.getInt(request.getParameter("crystal"));
		int runeNum = StringUtil.getInt(request.getParameter("runeNum"));
		int power = StringUtil.getInt(request.getParameter("power"));
		int friendNum = StringUtil.getInt(request.getParameter("friendNum"));
		
		if (!isValidMailAttachField(rewardType1, rewardId1, rewardNumber1))
		{
			request.setAttribute("msg", "设置失败,请检查第一个附带物品数据");
			return new ModelAndView("zcmail/zcmail.vm");
		}
		if (!isValidMailAttachField(rewardType2, rewardId2, rewardNumber2))
		{
			request.setAttribute("msg", "设置失败,请检查第二个附带物品数据");
			return new ModelAndView("zcmail/zcmail.vm");
		}
		if (!isValidMailAttachField(rewardType3, rewardId3, rewardNumber3))
		{
			request.setAttribute("msg", "设置失败,请检查第三个附带物品数据");
			return new ModelAndView("zcmail/zcmail.vm");
		}
		if (!isValidMailAttachField(rewardType4, rewardId4, rewardNumber4))
		{
			request.setAttribute("msg", "设置失败,请检查第四个附带物品数据");
			return new ModelAndView("zcmail/zcmail.vm");
		}
		if (!isValidMailAttachField(rewardType5, rewardId5, rewardNumber5))
		{
			request.setAttribute("msg", "设置失败,请检查第五个附带物品数据");
			return new ModelAndView("zcmail/zcmail.vm");
		}
		if (!isValidMailAttachField(rewardType6, rewardId6, rewardNumber6))
		{
			request.setAttribute("msg", "设置失败,请检查第六个附带物品数据");
			return new ModelAndView("zcmail/zcmail.vm");
		}
		// String sender = "GM";
		String reward1 = rewardType1 + "&" + rewardId1 + "&" + rewardNumber1;
		String reward2 = rewardType2 + "&" + rewardId2 + "&" + rewardNumber2;
		String reward3 = rewardType3 + "&" + rewardId3 + "&" + rewardNumber3;
		String reward4 = rewardType4 + "&" + rewardId4 + "&" + rewardNumber4;
		String reward5 = rewardType5 + "&" + rewardId5 + "&" + rewardNumber5;
		String reward6 = rewardType6 + "&" + rewardId6 + "&" + rewardNumber6;
		
		int deleteDay = StringUtil.getInt(request.getParameter("deleteDay"));
		int deleteHour = StringUtil.getInt(request.getParameter("deleteHour"));
		int deleteMinute = StringUtil.getInt(request.getParameter("deleteMinute"));
		int deleteTime = deleteDay * 24 * 60 + deleteHour * 60 + deleteMinute;
		
		int type = StringUtil.getInt(request.getParameter("type"));
		ZcMail zcMail = null;
		if (type == 1)
		{
			zcMail = ZcMail.createFirstLoginMail(title, content, reward1, reward2, reward3, reward4, reward5, reward6, gold, crystal, runeNum, power, friendNum, deleteTime);
			managerLogger.info(getManager(request).getName() + "|配置了首次登录邮件" + "|邮件标题:" + zcMail.getTitle() + "|登录IP：" + request.getRemoteAddr());
		}
		else
		{
			zcMail = ZcMail.createFirstPayMail(title, content, reward1, reward2, reward3, reward4, reward5, reward6, gold, crystal, runeNum, power, friendNum, deleteTime);
			managerLogger.info(getManager(request).getName() + "|配置了首次充值邮件" + "|邮件标题:" + zcMail.getTitle() + "|登录IP：" + request.getRemoteAddr());
		}
		commDao.saveZcMail(zcMail);
		return allZcMail(request, response);
	}
	
	public ModelAndView allZcMail(HttpServletRequest request, HttpServletResponse response)
	{
		List<ZcMail> list = commDao.find();
		request.setAttribute("list", list);
		return new ModelAndView("zcmail/all.vm");
	}
	
	public ModelAndView openOrCloseZcMail(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		int id = Integer.parseInt(request.getParameter("id"));
		ZcMail zcMail = commDao.oneZcMail(id);
		int state = zcMail.getState();
		if (state == 1)
		{
			zcMail.setState(0);
			managerLogger.info(getManager(request).getName() + "|关闭开服邮件" + "|邮件标题:" + zcMail.getTitle() + "|登录IP：" + request.getRemoteAddr());
		}
		if (state == 0)
		{
			zcMail.setState(1);
			managerLogger.info(getManager(request).getName() + "|开启开服邮件" + "|邮件标题:" + zcMail.getTitle() + "|登录IP：" + request.getRemoteAddr());
		}
		commDao.upZcMailState(zcMail);
		return allZcMail(request, response);
	}
	
	public ModelAndView delZcMail(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		int id = Integer.parseInt(request.getParameter("id"));
		ZcMail zcMail = new ZcMail();
		zcMail.setId(id);
		commDao.delZcMail(zcMail);
		managerLogger.info(getManager(request).getName() + "|删除开服邮件" + "|条件ID：" + id + "|登录IP：" + request.getRemoteAddr());
		return allZcMail(request, response);
	}
	
	public ModelAndView reloadBins(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		String reloadKey = request.getParameter("reloadKey");
		if ("9d33o2gm&#LJ@KNNasd./,2".equals(reloadKey))
		{
			new BinReader().readAllData();
			request.setAttribute("msg", "重载完毕");
			managerLogger.info(getManager(request).getName() + "|重载bin文件|登录IP：" + request.getRemoteAddr());
		}
		else
		{
			request.setAttribute("msg", "请输入正确的密钥");
		}
		return new ModelAndView("reloadBin.jsp");
	}
	
	/** 检查邮件附件字段数据是否有效 **/
	private boolean isValidMailAttachField(int rewardType, int rewardId, int rewardNumber)
	{
		if (rewardType == 1 && rewardId != 0 && (ItemsData.getItemsData(rewardId) == null || rewardNumber <= 0))
		{
			return false;
		}
		if (rewardType == 2 && rewardId != 0 && (EquipData.getData(rewardId) == null || rewardNumber <= 0))
		{
			return false;
		}
		if (rewardType == 3 && rewardId != 0 && (CardData.getData(rewardId) == null || rewardNumber <= 0))
		{
			return false;
		}
		if (rewardType == 4 && rewardId != 0 && (SkillData.getData(rewardId) == null || rewardNumber <= 0))
		{
			return false;
		}
		if (rewardType == 5 && rewardId != 0 && (PassiveSkillData.getData(rewardId) == null || rewardNumber <= 0))
		{
			return false;
		}
		return true;
	}
	
	public ModelAndView getAllEventDrop(HttpServletRequest request, HttpServletResponse response)
	{
		List<EventDrop> list = commDao.findEventDrops();
		request.setAttribute("list", list);
		return new ModelAndView("probability/allEventDrop.vm");
	}
	
	public ModelAndView eventDrop(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		return new ModelAndView("probability/eventDrop.vm");
	}
	
	public ModelAndView addEventDrop(HttpServletRequest request, HttpServletResponse response)
	{
		String startDay = request.getParameter("startDay");
		String endDay = request.getParameter("endDay");
		int eventType = StringUtil.getInt(request.getParameter("eventType"));
		int rewardType1 = StringUtil.getInt(request.getParameter("rewardType1"));
		int rewardId1 = StringUtil.getInt(request.getParameter("rewardId1"));
		int rewardNumber1 = StringUtil.getInt(request.getParameter("rewardNumber1"));
		int proNum1 = StringUtil.getInt(request.getParameter("proNum1"));
		int rewardType2 = StringUtil.getInt(request.getParameter("rewardType2"));
		int rewardId2 = StringUtil.getInt(request.getParameter("rewardId2"));
		int rewardNumber2 = StringUtil.getInt(request.getParameter("rewardNumber2"));
		int proNum2 = StringUtil.getInt(request.getParameter("proNum2"));
		int rewardType3 = StringUtil.getInt(request.getParameter("rewardType3"));
		int rewardId3 = StringUtil.getInt(request.getParameter("rewardId3"));
		int rewardNumber3 = StringUtil.getInt(request.getParameter("rewardNumber3"));
		int proNum3 = StringUtil.getInt(request.getParameter("proNum3"));
		int rewardType4 = StringUtil.getInt(request.getParameter("rewardType4"));
		int rewardId4 = StringUtil.getInt(request.getParameter("rewardId4"));
		int rewardNumber4 = StringUtil.getInt(request.getParameter("rewardNumber4"));
		int proNum4 = StringUtil.getInt(request.getParameter("proNum4"));
		int rewardType5 = StringUtil.getInt(request.getParameter("rewardType5"));
		int rewardId5 = StringUtil.getInt(request.getParameter("rewardId5"));
		int rewardNumber5 = StringUtil.getInt(request.getParameter("rewardNumber5"));
		int proNum5 = StringUtil.getInt(request.getParameter("proNum5"));
		int rewardType6 = StringUtil.getInt(request.getParameter("rewardType6"));
		int rewardId6 = StringUtil.getInt(request.getParameter("rewardId6"));
		int rewardNumber6 = StringUtil.getInt(request.getParameter("rewardNumber6"));
		int proNum6 = StringUtil.getInt(request.getParameter("proNum6"));
		if (!isValidMailAttachField(rewardType1, rewardId1, rewardNumber1))
		{
			request.setAttribute("msg", "设置失败,请检查第一个附带物品数据");
			return new ModelAndView("probability/eventDrop.vm");
		}
		if (!isValidMailAttachField(rewardType2, rewardId2, rewardNumber2))
		{
			request.setAttribute("msg", "设置失败,请检查第二个附带物品数据");
			return new ModelAndView("probability/eventDrop.vm");
		}
		if (!isValidMailAttachField(rewardType3, rewardId3, rewardNumber3))
		{
			request.setAttribute("msg", "设置失败,请检查第三个附带物品数据");
			return new ModelAndView("probability/eventDrop.vm");
		}
		if (!isValidMailAttachField(rewardType4, rewardId4, rewardNumber4))
		{
			request.setAttribute("msg", "设置失败,请检查第四个附带物品数据");
			return new ModelAndView("probability/eventDrop.vm");
		}
		if (!isValidMailAttachField(rewardType5, rewardId5, rewardNumber5))
		{
			request.setAttribute("msg", "设置失败,请检查第五个附带物品数据");
			return new ModelAndView("probability/eventDrop.vm");
		}
		if (!isValidMailAttachField(rewardType6, rewardId6, rewardNumber6))
		{
			request.setAttribute("msg", "设置失败,请检查第六个附带物品数据");
			return new ModelAndView("probability/eventDrop.vm");
		}
		String reward1 = rewardType1 + "&" + rewardId1 + "&" + rewardNumber1 + "&" + proNum1;
		String reward2 = rewardType2 + "&" + rewardId2 + "&" + rewardNumber2 + "&" + proNum2;
		String reward3 = rewardType3 + "&" + rewardId3 + "&" + rewardNumber3 + "&" + proNum3;
		String reward4 = rewardType4 + "&" + rewardId4 + "&" + rewardNumber4 + "&" + proNum4;
		String reward5 = rewardType5 + "&" + rewardId5 + "&" + rewardNumber5 + "&" + proNum5;
		String reward6 = rewardType6 + "&" + rewardId6 + "&" + rewardNumber6 + "&" + proNum6;
		EventDrop eventDrop = EventDrop.creatEventDrop(eventType, startDay, endDay, reward1, reward2, reward3, reward4, reward5, reward6);
		commDao.saveEventDrop(eventDrop);
		Cache.getInstance().initEventDrop();
		managerLogger.info(getManager(request).getName() + "|添加了全局掉落|type:" + eventType + "|登录IP：" + request.getRemoteAddr());
		return getAllEventDrop(request, response);
	}
	
	public ModelAndView delEventDrop(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		int id = StringUtil.getInt(request.getParameter("id"));
		EventDrop eventDrop = new EventDrop();
		eventDrop.setId(id);
		commDao.delEventDrop(eventDrop);
		Cache.getInstance().initEventDrop();
		managerLogger.info(getManager(request).getName() + "|删除了全局掉落|ID:" + id + "|登录IP：" + request.getRemoteAddr());
		return getAllEventDrop(request, response);
	}
	
	public ModelAndView skipUpEventDrop(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		int id = StringUtil.getInt(request.getParameter("id"));
		EventDrop eventDrop = commDao.findOneEventDrop(id);
		if (eventDrop != null)
		{
			String[] reward1 = eventDrop.getReward1().split("&");
			String[] reward2 = eventDrop.getReward2().split("&");
			String[] reward3 = eventDrop.getReward3().split("&");
			String[] reward4 = eventDrop.getReward4().split("&");
			String[] reward5 = eventDrop.getReward5().split("&");
			String[] reward6 = eventDrop.getReward6().split("&");
			PublicMailView publicMailView = new PublicMailView(Integer.parseInt(reward1[0]), Integer.parseInt(reward1[1]), Integer.parseInt(reward1[2]), Integer.parseInt(reward1[3]), Integer.parseInt(reward2[0]), Integer.parseInt(reward2[1]), Integer.parseInt(reward2[2]), Integer.parseInt(reward2[3]), Integer.parseInt(reward3[0]), Integer.parseInt(reward3[1]), Integer.parseInt(reward3[2]), Integer.parseInt(reward3[3]), Integer.parseInt(reward4[0]), Integer.parseInt(reward4[1]), Integer.parseInt(reward4[2]), Integer.parseInt(reward4[3]), Integer.parseInt(reward5[0]), Integer.parseInt(reward5[1]), Integer.parseInt(reward5[2]), Integer.parseInt(reward5[3]), Integer.parseInt(reward6[0]), Integer.parseInt(reward6[1]), Integer.parseInt(reward6[2]), Integer.parseInt(reward6[3]));
			request.setAttribute("eventDrop", eventDrop);
			request.setAttribute("publicMailView", publicMailView);
		}
		else
		{
			logger.info("eventDrop 为空！");
		}
		return new ModelAndView("probability/upEventDrop.vm");
	}
	
	public ModelAndView upEventDrop(HttpServletRequest request, HttpServletResponse response)
	{
		int id = StringUtil.getInt(request.getParameter("id"));
		String startDay = request.getParameter("startDay");
		String endDay = request.getParameter("endDay");
		int eventType = StringUtil.getInt(request.getParameter("eventType"));
		int rewardType1 = StringUtil.getInt(request.getParameter("rewardType1"));
		int rewardId1 = StringUtil.getInt(request.getParameter("rewardId1"));
		int rewardNumber1 = StringUtil.getInt(request.getParameter("rewardNumber1"));
		int proNum1 = StringUtil.getInt(request.getParameter("proNum1"));
		int rewardType2 = StringUtil.getInt(request.getParameter("rewardType2"));
		int rewardId2 = StringUtil.getInt(request.getParameter("rewardId2"));
		int rewardNumber2 = StringUtil.getInt(request.getParameter("rewardNumber2"));
		int proNum2 = StringUtil.getInt(request.getParameter("proNum2"));
		int rewardType3 = StringUtil.getInt(request.getParameter("rewardType3"));
		int rewardId3 = StringUtil.getInt(request.getParameter("rewardId3"));
		int rewardNumber3 = StringUtil.getInt(request.getParameter("rewardNumber3"));
		int proNum3 = StringUtil.getInt(request.getParameter("proNum3"));
		int rewardType4 = StringUtil.getInt(request.getParameter("rewardType4"));
		int rewardId4 = StringUtil.getInt(request.getParameter("rewardId4"));
		int rewardNumber4 = StringUtil.getInt(request.getParameter("rewardNumber4"));
		int proNum4 = StringUtil.getInt(request.getParameter("proNum4"));
		int rewardType5 = StringUtil.getInt(request.getParameter("rewardType5"));
		int rewardId5 = StringUtil.getInt(request.getParameter("rewardId5"));
		int rewardNumber5 = StringUtil.getInt(request.getParameter("rewardNumber5"));
		int proNum5 = StringUtil.getInt(request.getParameter("proNum5"));
		int rewardType6 = StringUtil.getInt(request.getParameter("rewardType6"));
		int rewardId6 = StringUtil.getInt(request.getParameter("rewardId6"));
		int rewardNumber6 = StringUtil.getInt(request.getParameter("rewardNumber6"));
		int proNum6 = StringUtil.getInt(request.getParameter("proNum6"));
		if (!isValidMailAttachField(rewardType1, rewardId1, rewardNumber1))
		{
			request.setAttribute("msg", "设置失败,请检查第一个附带物品数据");
			return new ModelAndView("probability/upEventDrop.vm");
		}
		if (!isValidMailAttachField(rewardType2, rewardId2, rewardNumber2))
		{
			request.setAttribute("msg", "设置失败,请检查第二个附带物品数据");
			return new ModelAndView("probability/upEventDrop.vm");
		}
		if (!isValidMailAttachField(rewardType3, rewardId3, rewardNumber3))
		{
			request.setAttribute("msg", "设置失败,请检查第三个附带物品数据");
			return new ModelAndView("probability/upEventDrop.vm");
		}
		if (!isValidMailAttachField(rewardType4, rewardId4, rewardNumber4))
		{
			request.setAttribute("msg", "设置失败,请检查第四个附带物品数据");
			return new ModelAndView("probability/upEventDrop.vm");
		}
		if (!isValidMailAttachField(rewardType5, rewardId5, rewardNumber5))
		{
			request.setAttribute("msg", "设置失败,请检查第五个附带物品数据");
			return new ModelAndView("probability/upEventDrop.vm");
		}
		if (!isValidMailAttachField(rewardType6, rewardId6, rewardNumber6))
		{
			request.setAttribute("msg", "设置失败,请检查第六个附带物品数据");
			return new ModelAndView("probability/upEventDrop.vm");
		}
		String reward1 = rewardType1 + "&" + rewardId1 + "&" + rewardNumber1 + "&" + proNum1;
		String reward2 = rewardType2 + "&" + rewardId2 + "&" + rewardNumber2 + "&" + proNum2;
		String reward3 = rewardType3 + "&" + rewardId3 + "&" + rewardNumber3 + "&" + proNum3;
		String reward4 = rewardType4 + "&" + rewardId4 + "&" + rewardNumber4 + "&" + proNum4;
		String reward5 = rewardType5 + "&" + rewardId5 + "&" + rewardNumber5 + "&" + proNum5;
		String reward6 = rewardType6 + "&" + rewardId6 + "&" + rewardNumber6 + "&" + proNum6;
		EventDrop eventDrop = EventDrop.creatEventDrop(eventType, startDay, endDay, reward1, reward2, reward3, reward4, reward5, reward6);
		eventDrop.setId(id);
		commDao.margeEventDrop(eventDrop);
		Cache.getInstance().initEventDrop();
		managerLogger.info(getManager(request).getName() + "|修改了全局掉落|ID:" + id + "|登录IP：" + request.getRemoteAddr());
		return getAllEventDrop(request, response);
	}
	
	public ModelAndView treasureCan(HttpServletRequest request, HttpServletResponse response)
	{
		int type = StringUtil.getInt(request.getParameter("type"));
		Activity activity = activityInfoDao.getTreasureCanDate(type);
		request.setAttribute("activity", activity);
		return new ModelAndView("treasureCan/treasureCan.vm");
	}
	
	public ModelAndView upTreasureCan(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		int activityId = StringUtil.getInt(request.getParameter("activityId"));
		String startDay = request.getParameter("startDay");
		String endDay = request.getParameter("endDay");
		Activity activity = activityInfoDao.getActivity(activityId);
		activity.setSttime(startDay);
		activity.setEndtime(endDay);
		activityInfoDao.upActivity(activity);
		Cache.getInstance().initActivity();
		request.setAttribute("msg", "修改成功！");
		managerLogger.info(getManager(request).getName() + "|修改了" + activity.activityName + "时间|登录IP：" + request.getRemoteAddr());
		request.setAttribute("activity", activity);
		return new ModelAndView("treasureCan/treasureCan.vm");
	}
	
	/** 模拟充值 **/
	public ModelAndView virtual_pay(HttpServletRequest request, HttpServletResponse response)
	{
		String mark = request.getParameter("mark");
		if ("1".equals(mark))
		{
			return new ModelAndView("virtual_pay.jsp");
		}
		String key = request.getParameter("key");
		if (!"ansfqongoqwnewengowewemgp2j023rwf".equals(key))
		{
			request.setAttribute("msg", "密钥错误");
			return new ModelAndView("virtual_pay.jsp");
		}
		int playerId = StringUtil.getInt(request.getParameter("playerId"));
		int rechargeId = StringUtil.getInt(request.getParameter("rechargeId"));
		RechargeData rd = RechargeData.getRechargeData(rechargeId);
		Player p = commDao.getPlayer(playerId);
		String msg = "充值失败";
		// if ((!"android".equals(p.getPlatform()) &&
		// !"ios".equals(p.getPlatform()) && !"pc".equals(p.getPlatform())))
		if (!"android".equals(p.getPlatform()) && !"ios".equals(p.getPlatform()))
		{
			msg += ",非手机用户不能充值";
			request.setAttribute("msg", msg);
			return new ModelAndView("virtual_pay.jsp");
		}
		if (p != null && rd != null)
		{
			notifyGameServer("1", playerId + "", rechargeId + "", rd.cost + "");
			msg = "充值成功" + rd.cost;
			managerLogger.info(getManager(request).getName() + "|模拟充值|登录IP：" + request.getRemoteAddr() + "|playerId:" + playerId + "|rechargeId:" + rechargeId);
		}
		request.setAttribute("msg", msg);
		return new ModelAndView("virtual_pay.jsp");
	}
	
	/**
	 * 通知游戏服务器 lt@2014-6-27 下午05:41:06
	 * 
	 * @param gameServerId
	 * @param playerId
	 * @param rechargeId
	 * @param consumeValue
	 */
	@SuppressWarnings("deprecation")
	public static void notifyGameServer(String gameServerId, String playerId, String rechargeId, String consumeValue)
	{
		try
		{
			// String myIp = "127.0.0.1:8080";
			String webIp = getWebIp();
			if (webIp == null)
			{
				managerLogger.info("没有获取到外网IP");
				return;
			}
			PayBackJson pb = new PayBackJson();
			pb.playerId = playerId;
			pb.rechargeId = rechargeId;
			pb.consumeValue = consumeValue;
			String json = JSON.toJSONString(pb);
			// aes加密,base64加密
			byte[] bytes = json.getBytes("utf-8");
			byte[] afterb = Base64.encodeBase64(aesEncryptWithKey(Cache.getInstance().getPay_game_key(), bytes, 0));
			String cipher = new String(afterb, "utf-8");
			String serverUrl = "http://" + webIp + "/card_server/receive_pay.htm?action=receive_pay";
			PostMethod req = new PostMethod(serverUrl);
			managerLogger.info("serverUrl:" + serverUrl);
			RequestEntity re = new StringRequestEntity(cipher);
			req.setRequestEntity(re);
			req.setRequestHeader("Content-type", "text/json; charset=UTF-8");
			HttpClient httpclient = new HttpClient();
			int result = httpclient.executeMethod(req);
			req.releaseConnection();
			managerLogger.info("向游戏服务器请求:" + result);
		}
		catch (Exception e)
		{
			managerLogger.error("向游戏服务器请求出错", e);
		}
	}
	
	private static byte[] aesEncryptWithKey(String key, byte[] input, int offset) throws Exception
	{
		byte[] put = new byte[input.length];
		System.arraycopy(input, offset, put, 0, input.length);
		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
		Cipher c = Cipher.getInstance("AES/ECB/PKCS5Padding");
		c.init(Cipher.ENCRYPT_MODE, skeySpec);
		byte[] encrypt = c.doFinal(put);
		return encrypt;
	}
	
	public static String getWebIp() throws Exception
	{
		String ip = null;
		String serverUrl = Cache.getInstance().getGift_code_server().replace("playerGetOneGift", "getWebIp");
		PostMethod req = new PostMethod(serverUrl);
		req.addParameter("serverId", Cache.getInstance().getServerId() + "");
		HttpClient httpclient = new HttpClient();
		int result = httpclient.executeMethod(req);
		if (result == 200)
		{
			ip = req.getResponseBodyAsString();
		}
		req.releaseConnection();
		return ip;
	}
	
	/**
	 * 特殊邮件获取合体技*
	 * 
	 * @throws IOException
	 */
	public ModelAndView getUnitSkillDatas(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		response.setCharacterEncoding("utf-8");
		StringBuilder sBuilder = new StringBuilder();
		List<UnitSkillData> unitSkillDatas = UnitSkillData.getAll();
		for (UnitSkillData unitSkillData : unitSkillDatas)
		{
			logger.info("unitSkillDatas" + unitSkillData.index);
			sBuilder.append("<option value='" + unitSkillData.index + "'>" + unitSkillData.index + "(" + unitSkillData.name + ")" + "</option>");
		}
		// System.out.println(sBuilder.toString());
		response.getWriter().write(sBuilder.toString());
		return null;
	}
	
	public ModelAndView setMaxSize(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		managerLogger.info("执行设置游戏服最大人数指令的GM账号:" + getManager(request).getName());
		String key = request.getParameter("key");
		if (!"msGAME#%ERSHOANQUAN~4TDO$SAVE".equals(key))
		{
			managerLogger.info("设置失败");
			request.setAttribute("result", "密钥错误，无法修改数据!");
			return new ModelAndView("user/result.vm");
		}
		int maxSize = StringUtil.getInt(request.getParameter("maxsize"));
		Cache.setMaxSize(maxSize);
		managerLogger.info("修改成功");
		request.setAttribute("result", "修改成功");
		return new ModelAndView("user/result.vm");
	}
	public ModelAndView setMaxSizeJsp(HttpServletRequest request,HttpServletResponse response)
	{
		request.setAttribute("maxSize", Cache.MaxSize);
		return new ModelAndView("maxsize.jsp");
	}
}

package com.begamer.card.model.pojo;

import com.begamer.card.common.util.StringUtil;
import com.begamer.card.json.element.MailUIResultElement;

public class Mail
{
	private int id;
	private int playerId;
	private String sender;
	private String title;
	private String content;
	private String reward1;//格式:type&id&number   type:1item  2equip  3card  4skill  5pskill 
	private String reward2;
	private String reward3;
	private String reward4;
	private String reward5;
	private String reward6;
	private int gold;
	private int crystal;
	private int runeNum;
	private int honor;
	private int diamond;
	private int power;
	private int friendNum;
	private int state;//1已删除,2未读邮件,0已读邮件
	private long sendTime;//单位毫秒
	private int deleteTime;//单位分钟
	
	public static Mail createMail(int playerId,String sender,String title,String content,String reward1,String reward2,String reward3,String reward4,String reward5,String reward6,int gold,int crystal,int runeNum,int power,int friendNum ,int deleteTime)
	{
		Mail mail=new Mail();
		mail.setPlayerId(playerId);
		mail.setSender(sender);
		mail.setTitle(title);
		mail.setContent(content);
		mail.setReward1(reward1);
		mail.setReward2(reward2);
		mail.setReward3(reward3);
		mail.setReward4(reward4);
		mail.setReward5(reward5);
		mail.setReward6(reward6);
		mail.setGold(gold);
		mail.setCrystal(crystal);
		mail.setRuneNum(runeNum);
		mail.setPower(power);
		mail.setFriendNum(friendNum);
		mail.setState(2);
		mail.setSendTime(System.currentTimeMillis());
		mail.setDeleteTime(deleteTime);
		return mail;
	}
	
	
	public static Mail createMail2(int playerId,String sender,String title,String content,String reward1,String reward2,String reward3,String reward4,String reward5,String reward6,int gold,int crystal,int runeNum,int power,int friendNum ,int deleteTime)
	{
		Mail mail=new Mail();
		mail.setPlayerId(playerId);
		mail.setSender(sender);
		mail.setTitle(title);
		mail.setContent(content);
		mail.setReward1(reward1);
		mail.setReward2(reward2);
		mail.setReward3(reward3);
		mail.setReward4(reward4);
		mail.setReward5(reward5);
		mail.setReward6(reward6);
		mail.setGold(gold);
		mail.setCrystal(crystal);
		mail.setRuneNum(runeNum);
		mail.setPower(power);
		mail.setFriendNum(friendNum);
		mail.setState(2);
		mail.setSendTime(System.currentTimeMillis());
		mail.setDeleteTime(deleteTime);
		return mail;
	}
	public int getDeleteTime()
	{
		return deleteTime;
	}

	public void setDeleteTime(int deleteTime)
	{
		this.deleteTime = deleteTime;
	}
	
	public boolean haveAttach()
	{
		if(reward1!=null && !"".equals(reward1))
		{
			String[] ss=reward1.split("&");
			int type=StringUtil.getInt(ss[0]);
			int rewardId=StringUtil.getInt(ss[1]);
			int number=StringUtil.getInt(ss[2]);
			if(type>0 && rewardId>0 && number>0)
			{
				return true;
			}
		}
		if(reward2!=null && !"".equals(reward2))
		{
			String[] ss=reward2.split("&");
			int type=StringUtil.getInt(ss[0]);
			int rewardId=StringUtil.getInt(ss[1]);
			int number=StringUtil.getInt(ss[2]);
			if(type>0 && rewardId>0 && number>0)
			{
				return true;
			}
		}
		if(reward3!=null && !"".equals(reward3))
		{
			String[] ss=reward3.split("&");
			int type=StringUtil.getInt(ss[0]);
			int rewardId=StringUtil.getInt(ss[1]);
			int number=StringUtil.getInt(ss[2]);
			if(type>0 && rewardId>0 && number>0)
			{
				return true;
			}
		}
		if(reward4!=null && !"".equals(reward4))
		{
			String[] ss=reward4.split("&");
			int type=StringUtil.getInt(ss[0]);
			int rewardId=StringUtil.getInt(ss[1]);
			int number=StringUtil.getInt(ss[2]);
			if(type>0 && rewardId>0 && number>0)
			{
				return true;
			}
		}
		if(reward5!=null && !"".equals(reward5))
		{
			String[] ss=reward5.split("&");
			int type=StringUtil.getInt(ss[0]);
			int rewardId=StringUtil.getInt(ss[1]);
			int number=StringUtil.getInt(ss[2]);
			if(type>0 && rewardId>0 && number>0)
			{
				return true;
			}
		}
		if(reward6!=null && !"".equals(reward6))
		{
			String[] ss=reward6.split("&");
			int type=StringUtil.getInt(ss[0]);
			int rewardId=StringUtil.getInt(ss[1]);
			int number=StringUtil.getInt(ss[2]);
			if(type>0 && rewardId>0 && number>0)
			{
				return true;
			}
		}
		if(gold>0)
		{
			return true;
		}
		if(crystal>0)
		{
			return true;
		}
		if(runeNum>0)
		{
			return true;
		}
		if(power>0)
		{
			return true;
		}
		if(friendNum>0)
		{
			return true;
		}
		if (diamond>0)
		{
			return true;
		}
		return false;
	}
	
	public void clearAttach()
	{
		reward1=null;
		reward2=null;
		reward3=null;
		reward4=null;
		reward5=null;
		reward6=null;
		gold=0;
		crystal=0;
		runeNum=0;
		diamond=0;
		power=0;
		friendNum=0;
	}
	
	//==邮件标题,发件人,发件时间(yyyy-MM-dd HH:ss),是否新邮件(2未读邮件,0已读邮件)==//
	public MailUIResultElement getJson()
	{
		MailUIResultElement me=new MailUIResultElement();
		me.title=title;
		me.sender=sender;
		me.sendTime=StringUtil.getMailDataTime(sendTime);
		me.mark=state;
		return me;
	}
	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public int getPlayerId()
	{
		return playerId;
	}
	public void setPlayerId(int playerId)
	{
		this.playerId = playerId;
	}
	public String getSender()
	{
		return sender;
	}
	public void setSender(String sender)
	{
		this.sender = sender;
	}
	public String getTitle()
	{
		return title;
	}
	public void setTitle(String title)
	{
		this.title = title;
	}
	public String getContent()
	{
		return content;
	}
	public void setContent(String content)
	{
		this.content = content;
	}
	public String getReward1()
	{
		return reward1;
	}
	public void setReward1(String reward1)
	{
		this.reward1 = reward1;
	}
	public String getReward2()
	{
		return reward2;
	}
	public void setReward2(String reward2)
	{
		this.reward2 = reward2;
	}
	public String getReward3()
	{
		return reward3;
	}
	public void setReward3(String reward3)
	{
		this.reward3 = reward3;
	}
	
	public String getReward4()
	{
		return reward4;
	}

	public void setReward4(String reward4)
	{
		this.reward4 = reward4;
	}

	public String getReward5()
	{
		return reward5;
	}

	public void setReward5(String reward5)
	{
		this.reward5 = reward5;
	}

	public String getReward6()
	{
		return reward6;
	}

	public void setReward6(String reward6)
	{
		this.reward6 = reward6;
	}

	public int getGold()
	{
		return gold;
	}
	public void setGold(int gold)
	{
		this.gold = gold;
	}
	public int getCrystal()
	{
		return crystal;
	}
	public void setCrystal(int crystal)
	{
		this.crystal = crystal;
	}
	public int getRuneNum()
	{
		return runeNum;
	}
	public void setRuneNum(int runeNum)
	{
		this.runeNum = runeNum;
	}
	public int getPower()
	{
		return power;
	}
	public void setPower(int power)
	{
		this.power = power;
	}
	public int getFriendNum()
	{
		return friendNum;
	}
	public void setFriendNum(int friendNum)
	{
		this.friendNum = friendNum;
	}
	public int getState()
	{
		return state;
	}
	public void setState(int state)
	{
		this.state = state;
	}
	public long getSendTime()
	{
		return sendTime;
	}
	public void setSendTime(long sendTime)
	{
		this.sendTime = sendTime;
	}

	public int getHonor() {
		return honor;
	}

	public void setHonor(int honor) {
		this.honor = honor;
	}

	public int getDiamond() {
		return diamond;
	}

	public void setDiamond(int diamond) {
		this.diamond = diamond;
	}
	
}

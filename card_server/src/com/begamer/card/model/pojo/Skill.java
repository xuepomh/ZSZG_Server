package com.begamer.card.model.pojo;

import org.apache.log4j.Logger;

import com.begamer.card.cache.Cache;
import com.begamer.card.common.util.binRead.SkillData;
import com.begamer.card.common.util.binRead.SkillExpData;
import com.begamer.card.log.PlayerLogger;


public class Skill {
	private static final Logger playerLogger=PlayerLogger.logger;
	
	private int id;
	private int playerId;
	private int skillId;
	private int level;
	private String createTime;
	private int sell;
	private int curExp;
	private int newType;
	
	public static Skill createSkill(int playerId,int skillId,int level)
	{
		Skill skill=new Skill();
		skill.setCreateTime(System.currentTimeMillis()+"");
		skill.setSkillId(skillId);
		skill.setPlayerId(playerId);
		skill.setLevel(level);
		skill.setCurExp(0);
		skill.setSell(0);
		skill.setNewType(0);
		return skill;
	}
	
	public Skill() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Integer playerId) {
		this.playerId = playerId;
	}

	public Integer getSkillId() {
		return skillId;
	}

	public void setSkillId(Integer skillId) {
		this.skillId = skillId;
	}

	public String getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(String createTime)
	{
		this.createTime = createTime;
	}

	public Integer getSell() {
		return sell;
	}

	public void setSell(Integer sell) {
		this.sell = sell;
	}

	public void setCurExp(Integer curExp)
	{
		this.curExp = curExp;
	}

	public Integer getCurExp()
	{
		return curExp;
	}
	
	public void addCurExp(int exp  , int mLevel)
	{
		playerLogger.info("|区服："+Cache.getInstance().serverId+"|技能卡："+skillId+"|等级："+level+"|获得经验|"+exp);
		SkillData skillData =SkillData.getData(skillId);
		if(skillData == null)
		{
			return;
		}
		while(exp>0)
		{
			SkillExpData sed =SkillExpData.getData(level+1);
			if(sed != null)
			{
				if(exp+curExp>=sed.starexps[skillData.star-1])
				{
					exp = exp+curExp-sed.starexps[skillData.star-1];
					if(level<mLevel)
					{
						level =level+1;
					}
					else
					{
						curExp =0;
						break;
					}
					curExp = 0;	
				}
				else
				{
					curExp = curExp+exp;
					exp = 0;
				}
			}
			else
			{
				curExp = 0;
				exp =0;
			}
		}
	}

	
	public void setId(int id)
	{
	
		this.id = id;
	}

	public void setLevel(int level)
	{
		this.level = level;
	}

	public int getLevel()
	{
		return level;
	}

	public int getNewType() {
		return newType;
	}

	public void setNewType(int newType) {
		this.newType = newType;
	}
	
}

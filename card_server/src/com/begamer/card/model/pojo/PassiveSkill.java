package com.begamer.card.model.pojo;

import org.apache.log4j.Logger;

import com.begamer.card.cache.Cache;
import com.begamer.card.common.util.binRead.PassiveSkillData;
import com.begamer.card.log.PlayerLogger;


public class PassiveSkill {
	private static final Logger playerLogger=PlayerLogger.logger;
	
	private int id;
	private int playerId;
	private int passiveSkillId;
	private String createTime;
	private int sell;
	private int curExp;
	private int newType;
	
	public static PassiveSkill createPassiveSkill(int playerId,int pSkillId)
	{
		PassiveSkill pskill=new PassiveSkill();
		pskill.setCreateTime(System.currentTimeMillis()+"");
		pskill.setPassiveSkillId(pSkillId);
		pskill.setPlayerId(playerId);
		pskill.setSell(0);
		pskill.setNewType(0);
		return pskill;
	}
	
	public PassiveSkill() {
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getPlayerId()
	{
		return playerId;
	}

	public void setPlayerId(int playerId)
	{
		this.playerId = playerId;
	}

	public int getPassiveSkillId()
	{
		return passiveSkillId;
	}

	public void setPassiveSkillId(int passiveSkillId)
	{
		this.passiveSkillId = passiveSkillId;
	}

	public int getSell()
	{
		return sell;
	}

	public void setSell(int sell)
	{
		this.sell = sell;
	}

	public String getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(String createTime)
	{
		this.createTime = createTime;
	}

	public int getCurExp() {
		return curExp;
	}

	public void setCurExp(int curExp) {
		this.curExp = curExp;
	}
	
	public void addCurExp(int exp)
	{
		playerLogger.info("|区服："+Cache.getInstance().serverId+"|被动技能卡："+passiveSkillId+"|获得经验|"+exp);
		PassiveSkillData passiveSkillData =PassiveSkillData.getData(passiveSkillId);
		if(passiveSkillData == null)
		{
			return;
		}
		while(exp>0)
		{
			PassiveSkillData psData =PassiveSkillData.getData(passiveSkillId+1);
			if(psData != null)
			{
				if(exp+curExp>=psData.exp)
				{
					passiveSkillId = passiveSkillId+1;
					exp = exp+curExp-psData.exp;
					curExp = 0;	
				}
				else
				{
					curExp = curExp+exp;
					exp = 0;
				}
			}
			else
			{
				curExp = 0;
				exp =0;
			}
		}
	}

	public int getNewType() {
		return newType;
	}

	public void setNewType(int newType) {
		this.newType = newType;
	}
	
}

package com.begamer.card.model.view;

public class EquipView {

	private int id;

	private String name;

	private int level;

	public static EquipView creatEquipView(int id,String name,int level)
	{
		EquipView equipView = new EquipView();
		equipView.setId(id);
		equipView.setName(name);
		equipView.setLevel(level);
		return equipView;
	}
	
	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getLevel()
	{
		return level;
	}

	public void setLevel(int level)
	{
		this.level = level;
	}

}

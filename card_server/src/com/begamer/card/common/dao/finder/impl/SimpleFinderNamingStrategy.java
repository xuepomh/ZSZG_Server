package com.begamer.card.common.dao.finder.impl;

import java.lang.reflect.Method;

import com.begamer.card.common.dao.finder.FinderNamingStrategy;

public class SimpleFinderNamingStrategy implements FinderNamingStrategy {
	public String queryNameFromMethod(Class<?> findTargetType,
			Method finderMethod) {
		return findTargetType.getSimpleName() + "." + finderMethod.getName();
	}
}
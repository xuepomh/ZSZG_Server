package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class SkillPropertyData implements PropertyReader
{
	//==种类:1：攻击和回复类型主动技能,2：防御类型主动技能==//
	public int type;
	//==等级==//
	public int level;
	//==1星数值==//
	public int starNumber1;
	//==2星数值==//
	public int starNumber2;
	//==3星数值==//
	public int starNumber3;
	//==4星数值==//
	public int starNumber4;
	//==5星数值==//
	public int starNumber5;
	//==6星数值==//
	public int starNumber6;
	
	public int[] starNumbers;
	
	private static HashMap<String, SkillPropertyData> data=new HashMap<String, SkillPropertyData>();
	
	public void addData()
	{
		starNumbers=new int[6];
		starNumbers[0]=starNumber1;
		starNumbers[1]=starNumber2;
		starNumbers[2]=starNumber3;
		starNumbers[3]=starNumber4;
		starNumbers[4]=starNumber5;
		starNumbers[5]=starNumber6;
		
		data.put(type+"-"+level,this);
	}
	public void resetData()
	{
		data.clear();
	}
	public void parse(String[] ss){}
	
	//==skillType:1,2,3==//
	public static int getProperty(int skillType,int level,int star)
	{
		return data.get(skillType+"-"+level).starNumbers[star-1];
	}
}

package com.begamer.card.model.pojo;

import com.begamer.card.common.util.StringUtil;
import com.begamer.card.json.PayJson;


public class PayInfo
{
	private int id;
	private String userId;//用户通行证账号
	private String userNumId;//用户通行证数字ID
	private String gameId;//游戏ID
	private long reqtime;//消费时间
	private String state;//消费状态：1 成功；0 失败
	private String consumeValue;//消费金额,单位元
	private String extraData;//附加信息,可以为空
	private String gameServerZone;//用户此次消费所在游戏服务器分区
	private String consumeId;//消费订单号
	private String reqDate;//消费时间
	private String gameOrderId;//游戏订单号
	
	public static PayInfo createPayInfo(PayJson pj)
	{
		PayInfo pi=new PayInfo();
		pi.setUserId(pj.getUserId());
		pi.setUserNumId(pj.getUserNumId());
		pi.setGameId(pj.getGameId());
		pi.setReqtime(pj.getReqtime());
		pi.setState(pj.getState());
		pi.setConsumeValue(pj.getConsumeValue());
		pi.setExtraData(pj.getExtraData());
		pi.setGameServerZone(pj.getGameServerZone());
		pi.setConsumeId(pj.getConsumeId());
		pi.setReqDate(StringUtil.getDateTime(pj.getReqtime()));
		return pi;
	}
	
	/**
	 * 云点友游
	 * lt@2014-6-27 下午04:57:37
	 * @param userId 用户通行证账号
	 * @param reqDate 消费时间
	 * @param state 消费状态：1 成功；0 失败
	 * @param consumeValue 消费金额,单位元
	 * @param extraData 附加信息,可以为空
	 * @param gameServerZone 用户此次消费所在游戏服务器分区
	 * @param consumeId 消费订单号
	 * @param gameOrderId 游戏订单号
	 * @return
	 */
	public static PayInfo createPayInfo(String userId,String reqDate,String state,String consumeValue,String extraData,String gameServerZone,String consumeId,String gameOrderId)
	{
		PayInfo pi=new PayInfo();
		pi.setUserId(userId);
		pi.setReqDate(reqDate);
		pi.setState(state);
		pi.setConsumeValue(consumeValue);
		pi.setExtraData(extraData);
		pi.setGameServerZone(gameServerZone);
		pi.setConsumeId(consumeId);
		pi.setGameOrderId(gameOrderId);
		return pi;
	}
	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public String getUserId()
	{
		return userId;
	}
	public void setUserId(String userId)
	{
		this.userId = userId;
	}
	public String getUserNumId()
	{
		return userNumId;
	}
	public void setUserNumId(String userNumId)
	{
		this.userNumId = userNumId;
	}
	public String getGameId()
	{
		return gameId;
	}
	public void setGameId(String gameId)
	{
		this.gameId = gameId;
	}
	public long getReqtime()
	{
		return reqtime;
	}
	public void setReqtime(long reqtime)
	{
		this.reqtime = reqtime;
	}
	public String getState()
	{
		return state;
	}
	public void setState(String state)
	{
		this.state = state;
	}
	public String getConsumeValue()
	{
		return consumeValue;
	}
	public void setConsumeValue(String consumeValue)
	{
		this.consumeValue = consumeValue;
	}
	public String getExtraData()
	{
		return extraData;
	}
	public void setExtraData(String extraData)
	{
		this.extraData = extraData;
	}
	public String getGameServerZone()
	{
		return gameServerZone;
	}
	public void setGameServerZone(String gameServerZone)
	{
		this.gameServerZone = gameServerZone;
	}
	public String getConsumeId()
	{
		return consumeId;
	}
	public void setConsumeId(String consumeId)
	{
		this.consumeId = consumeId;
	}
	public String getReqDate()
	{
		return reqDate;
	}
	public void setReqDate(String reqDate)
	{
		this.reqDate = reqDate;
	}
	public String getGameOrderId()
	{
		return gameOrderId;
	}
	public void setGameOrderId(String gameOrderId)
	{
		this.gameOrderId = gameOrderId;
	}
}

package com.begamer.card.json;

public class VersionJson
{
	public String versionId;
	public String channelId;
	public String platform;

	public String getVersionId()
	{
		return versionId;
	}

	public void setVersionId(String versionId)
	{
		this.versionId = versionId;
	}

	public String getChannelId()
	{
		return channelId;
	}

	public void setChannelId(String channelId)
	{
		this.channelId = channelId;
	}

	public String getPlatform()
	{
		return platform;
	}

	public void setPlatform(String platform)
	{
		this.platform = platform;
	}

}
